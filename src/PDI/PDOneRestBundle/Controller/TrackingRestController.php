<?php

/**
 * RestAPI:       Events Tracking.
 *
 * @author        Reynier Perez <reynier@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneRestBundle\Controller;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use PDI\PDOneBundle\Entity\Event;
use Symfony\Component\HttpFoundation\Response;

class TrackingRestController extends FOSRestController
{
    /**
     * Tracks when email was opened.
     *
     * @param ParamFetcher $paramFetcher
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "Get representative details.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @QueryParam(name="eid", nullable=false, strict=true, description="The encrypted ID of the
     *                         email")
     *
     * @return View
     */
    public function getTrackingEmailAction(ParamFetcher $paramFetcher)
    {
        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $eid = $this->get('nzo_url_encryptor')->decrypt($paramFetcher->get('eid'));

        $entTargetEmailRep = $em->getRepository('PDOneBundle:Representative')->findTargetEmailRepValues($eid);
        $entBrand = $em->getRepository('PDOneBundle:Representative')->findBrandValues($eid);

        if ($entTargetEmailRep === null) {
            return new Response(
                base64_decode(
                    'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII='
                ), 200, ['Content-Type' => 'image/png']
            );
        }

        $track = $em->getRepository('PDOneBundle:Event')->findOneBy(
            array(
                'event_type' => 'email_view',
                'rep_id' => $entTargetEmailRep['rep_id'],
                'target_id' => $entTargetEmailRep['target_id'],
                'brand_id' => $entBrand,
                'email_id' => $eid,
            ),
            array('createdAt' => 'DESC')
        );

        if ($track === null) {
            $entEvent = new Event();
            $entEvent->setEmailId($eid);
            $entEvent->setBrandId($entBrand);
            $entEvent->setRepId($entTargetEmailRep['rep_id']);
            $entEvent->setTargetId($entTargetEmailRep['target_id']);
            $entEvent->setEventType('email_view');

            $em->persist($entEvent);
        }

        try {
            if ($track === null) {
                $em->flush();
            }

            return new Response(
                base64_decode(
                    'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII='
                ), 200, ['Content-Type' => 'image/png']
            );
        } catch (\PDOException $e) {
            $response = [
                'error' => $e->getMessage(),
            ];
        }

        $view->setData($response)->setStatusCode(200);

        return $view;
    }
}

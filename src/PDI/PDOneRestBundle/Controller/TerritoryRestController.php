<?php

/**
 * RestAPI:       Territory.
 *
 * @author        Reynier Perez <reynier@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneRestBundle\Controller;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class TerritoryRestController extends FOSRestController
{
    /**
     * List all targets for territory.
     *
     * @param ParamFetcher $paramFetcher
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "List all targets for territory.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @QueryParam(name="tid", nullable=false, strict=true, description="The ID of the territory")
     * Get("/territories/{tid}/targets")
     *
     * @return View
     */
    public function getTerritoriesTargetsAction(ParamFetcher $paramFetcher)
    {
        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('PDOneBundle:Territory')->getTerritoryTargets($paramFetcher->get('tid'));

        if (!$entities) {
            $view->setData(array())->setStatusCode(200);

            return $view;
        }

        $targets = array();

        foreach ($entities as $entity) {
            $email_status = $em->getRepository('PDOneBundle:Territory')->getTerritoryTargetsEmailStatus($entity['target_id'], $entity['email']);

            $targets[] = [
                'id' => $entity['target_id'],
                'territories_id' => $entity['territories_id'],
                'veeva_timestamp' => $entity['veeva_timestamp'],
                'display_name' => $entity['display_name'],
                'avatar_url' => $entity['avatar_url'],
                'title' => $entity['title'],
                'first' => $entity['first'],
                'last' => $entity['last'],
                'suffix' => $entity['suffix'],
                'address1' => $entity['address1'],
                'address2' => $entity['address2'],
                'city' => $entity['city'],
                'state' => $entity['state'],
                'zip' => $entity['zip'],
                'phone' => $entity['phone'],
                'fax' => $entity['fax'],
                'email' => $entity['email'],
                'state_licensed' => $entity['state_licensed'],
                'state_licensed_id' => $entity['state_licensed_id'],
                'target_type' => $entity['target_type'],
                'npi' => $entity['npi'],
                'practioner_id' => $entity['practioner_id'],
                'inactive' => $entity['inactive'],
                'lastSyncAt' => $entity['updatedAt'],
                'createdAt' => $entity['createdAt'],
                'updatedAt' => $entity['updatedAt'],
                'email_status' => ($email_status) ? $email_status['action_type'] : null,
            ];
        }

        $view->setData($targets)->setStatusCode(200);

        return $view;
    }

    /**
     * List all contents for territory.
     *
     * @param ParamFetcher $paramFetcher
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "List all contents for territory.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @QueryParam(name="tid", nullable=false, strict=true, description="The ID of the territory")
     *
     * @return View
     */
    public function getTerritoriesContentAction(ParamFetcher $paramFetcher)
    {
        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $entAllCompanies = $em->getRepository('PDOneBundle:Territory')->getAllCompanies($paramFetcher->get('tid'));
        $entAllBrands = $em->getRepository('PDOneBundle:Territory')->getAllBrands($paramFetcher->get('tid'));
        $entAllMedia = $em->getRepository('PDOneBundle:Territory')->getAllMedia($paramFetcher->get('tid'));
        $entAllMessages = $em->getRepository('PDOneBundle:Territory')->getAllMessages($paramFetcher->get('tid'));

        $companies = $brands = $messages = $medias = [];
        if (!$entAllCompanies) {
            $companies = [];
        } else {
            for ($i = 0; $i < count($entAllCompanies); ++$i) {
                $companies[] = [
                    'id' => $entAllCompanies[$i]['id'],
                    'company_id' => $entAllCompanies[$i]['id'],
                    'name' => $entAllCompanies[$i]['name'],
                    'logo_url' => $entAllCompanies[$i]['logo_url'],
                    'division' => $entAllCompanies[$i]['division'],
                    'inactive' => $entAllCompanies[$i]['inactive'],
                    'createdAt' => $entAllCompanies[$i]['createdAt'],
                    'updatedAt' => $entAllCompanies[$i]['updatedAt'],
                ];
            }
        }

        if (!$entAllBrands) {
            $brands = [];
        } else {
            for ($j = 0; $j < count($entAllBrands); ++$j) {
                $brands[] = [
                    'id' => $entAllBrands[$j]['id'],
                    'company_id' => $entAllBrands[$j]['companies_id'],
                    'name' => $entAllBrands[$j]['name'],
                    'generic_name' => $entAllBrands[$j]['generic_name'],
                    'priority' => $entAllBrands[$j]['priority'],
                    'logo_url' => $entAllBrands[$j]['logo_url'],
                    'description' => $entAllBrands[$j]['description'],
                    'isi_required' => $entAllBrands[$j]['isi_required'],
                    'isi_text' => $entAllBrands[$j]['isi_text'],
                    'isi_pdf_url' => $entAllBrands[$j]['isi_pdf_url'],
                    'pi_required' => $entAllBrands[$j]['pi_required'],
                    'pi_text' => $entAllBrands[$j]['pi_text'],
                    'pi_pdf_url' => $entAllBrands[$j]['pi_pdf_url'],
                    'inactive' => $entAllBrands[$j]['inactive'],
                    'createdAt' => $entAllBrands[$j]['createdAt'],
                    'updatedAt' => $entAllBrands[$j]['updatedAt'],
                ];
            }
        }

        if (!$entAllMedia) {
            $medias = [];
        } else {
            $entReps = $em->getRepository('PDOneBundle:Representative')->findOneBy(
                array(
                    'territory' => $paramFetcher->get('tid'),
                )
            );

            $medias = [];
            for ($l = 0; $l < count($entAllMedia); ++$l) {
                $medias[] = [
                    'id' => $entAllMedia[$l]['id'],
                    'brands_id' => $entAllMedia[$l]['brands_id'],
                    'media_type' => $entAllMedia[$l]['media_type'],
                    'title' => $entAllMedia[$l]['title'],
                    'description' => $entAllMedia[$l]['description'],
                    'thumbnail_url' => $entAllMedia[$l]['thumbnail_url'],
                    'media_url' => $entAllMedia[$l]['media_url'],
                    'media_code' => $entAllMedia[$l]['media_code'],
                    'total_shares' => (string) $em->getRepository('PDOneBundle:Representative')->getTotalMediaShares(
                        $entReps->getVeevaRepId(),
                        $entAllMedia[$l]['id']
                    ), // repId
                    'total_views' => (string) $em->getRepository('PDOneBundle:Representative')->getTotalMediaViews(
                        $entReps->getVeevaRepId(),
                        $entAllMedia[$l]['id']
                    ),
                    'inactive' => $entAllMedia[$l]['inactive'],
                    'createdAt' => $entAllMedia[$l]['createdAt'],
                    'updatedAt' => $entAllMedia[$l]['updatedAt'],
                    'last_shared' => $em->getRepository(
                        'PDOneBundle:Representative'
                    )->getLastMediaCreatedAtByEventType(
                        $entReps->getVeevaRepId(),
                        $entAllMedia[$l]['id'],
                        'media_share'
                    ) ? $em->getRepository('PDOneBundle:Representative')->getLastMediaCreatedAtByEventType(
                        $entReps->getVeevaRepId(),
                        $entAllMedia[$l]['id'],
                        'media_share'
                    ) : '0000-00-00 00:00:00',
                    'last_viewed' => $em->getRepository(
                        'PDOneBundle:Representative'
                    )->getLastMediaCreatedAtByEventType(
                        $entReps->getVeevaRepId(),
                        $entAllMedia[$l]['id'],
                        'media_view'
                    ) ? $em->getRepository('PDOneBundle:Representative')->getLastMediaCreatedAtByEventType(
                        $entReps->getVeevaRepId(),
                        $entAllMedia[$l]['id'],
                        'media_view'
                    ) : '0000-00-00 00:00:00',
                ];
            }
        }

        if (!$entAllMessages) {
            $messages = [];
        } else {
            for ($k = 0; $k < count($entAllMessages); ++$k) {
                $messages[] = [
                    'id' => $entAllMessages[$k]['id'],
                    'brands_id' => $entAllMessages[$k]['brands_id'],
                    'message_text' => $entAllMessages[$k]['message_text'],
                    'message_subject' => $entAllMessages[$k]['message_subject'],
                    'message_code' => $entAllMessages[$k]['message_code'],
                    'share_required' => $entAllMessages[$k]['content_share_required'],
                    'category' => $entAllMessages[$k]['category'],
                    'inactive' => $entAllMessages[$k]['inactive'],
                    'createdAt' => $entAllMessages[$k]['createdAt'],
                    'updatedAt' => $entAllMessages[$k]['updatedAt'],
                ];
            }
        }

        $respObject = [
            'companies' => $companies,
            'brands' => $brands,
            'messages' => $messages,
            'media' => $medias,
        ];

        $view->setData($respObject)->setStatusCode(200);

        return $view;
    }
}

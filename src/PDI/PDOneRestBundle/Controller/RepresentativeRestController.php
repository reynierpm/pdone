<?php

/**
 * RestAPI:       Representative.
 *
 * @author        Reynier Perez <reynier@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneRestBundle\Controller;

use Aws\Common\Credentials\Credentials;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

class RepresentativeRestController extends FOSRestController
{
    /**
     * Get representative details.
     *
     * @param ParamFetcher $paramFetcher
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "Get representative details.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @QueryParam(name="rid", nullable=false, strict=true, description="The ID of the representative")
     *
     * @return View
     */
    public function getRepsAction(ParamFetcher $paramFetcher)
    {
        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PDOneBundle:Representative')->find($paramFetcher->get('rid'));

        if (!$entity) {
            $view->setData(array())->setStatusCode(200);

            return $view;
        }

        $repsData = [
            'id' => $paramFetcher->get('rid'),
            'veeva_rep_id' => $entity->getVeevaRepId(),
            'display_name' => $entity->getDisplayName(),
            'avatar_url' => $entity->getAvatarUrl(),
            'rep_type' => $entity->getRepType(),
            'username' => $entity->getUsername(),
            'first' => $entity->getFirst(),
            'last' => $entity->getLast(),
            'title' => $entity->getTitle(),
            'phone' => $entity->getPhone(),
            'email' => $entity->getEmail(),
            'territory_id' => $entity->getTerritory() != null ? (string) $entity->getTerritory()->getTerritoryId() : '',
            'territory_name' => $entity->getTerritory() != null ? (string) $entity->getTerritory()->getName() : '',
            'inactive' => $entity->getInactive(),
            'total_contacts' => $em->getRepository('PDOneBundle:Representative')->getTotalContacts(
                $paramFetcher->get('rid')
            ),
            'total_shares' => $em->getRepository('PDOneBundle:Representative')->getTotalShares(
                $paramFetcher->get('rid')
            ),
            'totalViews' => $em->getRepository('PDOneBundle:Representative')->getTotalViews(
                $paramFetcher->get('rid')
            ),
            'lastLoginAt' => $entity->getLastLoginAt(),
            'lastVeevaSyncAt' => $entity->getUpdatedAt(),
            'createdAt' => $entity->getCreatedAt(),
            'updatedAt' => $entity->getUpdatedAt(),
            'app_display_url' => '',
        ];

        $view->setData($repsData)->setStatusCode(200);

        return $view;
    }

    /**
     * Update details for reps.
     *
     * @param ParamFetcher $paramFetcher
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "Update details for reps.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @RequestParam(name="rid", nullable=false, strict=true, description="The ID of the representative")
     * @RequestParam(name="displayname", nullable=false, strict=true, description="The display name value to update")
     *
     * @return View
     */
    public function putRepsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $view = View::create();

        $entReps = $em->getRepository('PDOneBundle:Representative')->find($paramFetcher->get('rid'));

        if (!$entReps) {
            $view->setData(array())->setStatusCode(200);

            return $view;
        }

        $entReps->setDisplayName($paramFetcher->get('displayname'));
        $em->flush();

        $repsData = [
            'id' => $paramFetcher->get('rid'),
            'veeva_rep_id' => $entReps->getVeevaRepId(),
            'display_name' => $entReps->getDisplayName(),
            'avatar_url' => $entReps->getAvatarUrl(),
            'rep_type' => $entReps->getRepType(),
            'username' => $entReps->getUsername(),
            'first' => $entReps->getFirst(),
            'last' => $entReps->getLast(),
            'title' => $entReps->getTitle(),
            'phone' => $entReps->getPhone(),
            'email' => $entReps->getEmail(),
            'territory_id' => $entReps->getTerritory()->getTerritoryId(),
            'inactive' => $entReps->getInactive(),
            'total_contacts' => $em->getRepository('PDOneBundle:Representative')->getTotalContacts(
                $paramFetcher->get('rid')
            ),
            'total_shares' => $em->getRepository('PDOneBundle:Representative')->getTotalShares(
                $paramFetcher->get('rid')
            ),
            'totalViews' => $em->getRepository('PDOneBundle:Representative')->getTotalViews(
                $paramFetcher->get('rid')
            ),
            'lastLoginAt' => $entReps->getLastLoginAt(),
            'lastVeevaSyncAt' => $entReps->getUpdatedAt(),
            'createdAt' => $entReps->getCreatedAt(),
            'updatedAt' => $entReps->getUpdatedAt(),
        ];

        $view->setData($repsData)->setStatusCode(200);

        return $view;
    }

    /**
     * Get representative activity.
     *
     * @param ParamFetcher $paramFetcher
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "Get all representatives.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @QueryParam(name="rid", nullable=false, strict=true, description="The ID of the representative")
     *
     * @return View
     */
    public function getRepsActivityAction(ParamFetcher $paramFetcher)
    {
        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $rep_activity = $share_details = [];

        $entRep = $em->getRepository('PDOneBundle:Representative')->find($paramFetcher->get('rid'));

        if (!$entRep) {
            $repsData = [
                'share_details' => $share_details,
                'rep_activity' => $rep_activity,
            ];

            $view->setData($repsData)->setStatusCode(200);

            return $view;
        }

        if ($entRep->getTerritory() != null) {
            $entShareDetails = $em->getRepository('PDOneBundle:Representative')->getAllMedia($entRep->getTerritory()->getTerritoryId());
            for ($i = 0; $i < count($entShareDetails); ++$i) {
                $share_details[] = [
                    'media_id' => $entShareDetails[$i]['id'],
                    'title' => $entShareDetails[$i]['title'],
                    'description' => $entShareDetails[$i]['description'],
                    'thumbnail_url' => $entShareDetails[$i]['thumbnail_url'],
                    'total_shares' => $em->getRepository('PDOneBundle:Representative')->getTotalMediaShares(
                        $paramFetcher->get('rid'),
                        $entShareDetails[$i]['id']
                    ),
                    'total_views' => $em->getRepository('PDOneBundle:Representative')->getTotalMediaViews(
                        $paramFetcher->get('rid'),
                        $entShareDetails[$i]['id']
                    ),
                    'last_shared' => $em->getRepository(
                        'PDOneBundle:Representative'
                    )->getLastMediaCreatedAtByEventType(
                        $paramFetcher->get('rid'),
                        $entShareDetails[$i]['id'],
                        'media_share'
                    ) ? $em->getRepository('PDOneBundle:Representative')->getLastMediaCreatedAtByEventType(
                        $paramFetcher->get('rid'),
                        $entShareDetails[$i]['id'],
                        'media_share'
                    ) : '0000-00-00 00:00:00',
                    'last_viewed' => $em->getRepository(
                        'PDOneBundle:Representative'
                    )->getLastMediaCreatedAtByEventType(
                        $paramFetcher->get('rid'),
                        $entShareDetails[$i]['id'],
                        'media_view'
                    ) ? $em->getRepository('PDOneBundle:Representative')->getLastMediaCreatedAtByEventType(
                        $paramFetcher->get('rid'),
                        $entShareDetails[$i]['id'],
                        'media_view'
                    ) : '0000-00-00 00:00:00',
                ];
            }
        }

        $entRepActivity = $em->getRepository('PDOneBundle:Representative')->getRepsActivity(
            $paramFetcher->get('rid')
        );
        $entRep = $em->getRepository('PDOneBundle:Representative')->find($paramFetcher->get('rid'));

        $mid = [6,13,14,15,16,17,18];
        for ($i = 0; $i < count($entRepActivity); ++$i) {
            switch ($entRepActivity[$i]['event_type']) {
                case 'email_sent':
                    $title = 'You contacted '.$entRepActivity[$i]['display_name'].'.';
                    $description = str_replace(
                        '%s',
                        $entRep->getDisplayName(),
                        $entRepActivity[$i]['message_subject']
                    );
                    $thumbnail = 'https://pdone.s3.amazonaws.com/activity/email_sent.png';
                    break;

                case 'email_view':
                    $title = $entRepActivity[$i]['display_name'].' viewed your message.';
                    $description = str_replace(
                        '%s',
                        $entRepActivity[$i]['display_name'],
                        $entRepActivity[$i]['message_subject']
                    );
                    $thumbnail = 'https://pdone.s3.amazonaws.com/activity/email_view.png';
                    break;

                case 'page_view':
                    $title = $entRepActivity[$i]['display_name'].' opened your email.';
                    $description = str_replace(
                        '%s',
                        $entRepActivity[$i]['display_name'],
                        $entRepActivity[$i]['message_text']
                    );
                    $thumbnail = 'https://pdone.s3.amazonaws.com/activity/page_view.png';
                    break;

                case 'media_view':
                    $title = $entRepActivity[$i]['display_name'].' viewed '.$entRepActivity[$i]['title'].'.';
                    $description = $entRepActivity[$i]['description'];
                    $thumbnail = 'https://pdone.s3.amazonaws.com/activity/media_view.png';
                    break;

                case 'rep_contact':
                    $title = $entRepActivity[$i]['display_name'].' contacted you.';

                    # fix for https://bitbucket.org/magnific/mt020415-00-pdonereptool/issue/57/return-phone-number-with-rep_contact
                    if (in_array($entRepActivity[$i]['mid'], $mid)) {
                        $description = str_replace(
                            '%s',
                            $entRepActivity[$i]['display_name'],
                            $entRepActivity[$i]['message_text']
                        );

                        $decode = json_decode($entRepActivity[$i]['data']);
                        $description .= "\nPlease call me at $decode->{'phone_number'}.";
                    } else {
                        $description = str_replace(
                            '%s',
                            $entRepActivity[$i]['display_name'],
                            $entRepActivity[$i]['message_text']
                        );
                    }

                    $thumbnail = 'https://pdone.s3.amazonaws.com/activity/rep_contact.png';
                    break;
            }

            $rep_activity[] = [
                'title' => $title,
                'description' => $description,
                'thumbnail' => $thumbnail,
                'timestamp' => $entRepActivity[$i]['createdAt'],
                'event_type' => $entRepActivity[$i]['event_type'],
            ];
        }

        $repsData = [
            'share_details' => $share_details,
            'rep_activity' => $rep_activity,
        ];

        $view->setData($repsData)->setStatusCode(200);

        return $view;
    }

    /**
     * List all emails for reps.
     *
     * @param ParamFetcher $paramFetcher
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "List all emails for reps.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @QueryParam(name="rid", nullable=false, strict=true, description="The ID of the representative")
     *
     * @return View
     */
    public function getRepsEmailsAction(ParamFetcher $paramFetcher)
    {
        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $entEmail = $em->getRepository('PDOneBundle:Representative')->getAllEmail($paramFetcher->get('rid'));
        $entRep = $em->getRepository('PDOneBundle:Representative')->find($paramFetcher->get('rid'));

        $response = [];
        if (!$entEmail) {
            $view->setData($response)->setStatusCode(200);

            return $view;
        }

        for ($i = 0; $i < count($entEmail); ++$i) {
            $entMedia = $em->getRepository('PDOneBundle:Representative')->getAllEmailsMedia($entEmail[$i]['id']);

            $media = [];
            if ($entMedia) {
                $mid = [6,13,14,15,16,17,18];

                for ($j = 0; $j < count($entMedia); ++$j) {
                    $media[] = [
                        'media_id' => $entMedia[$j]['id'],
                        'title' => $entMedia[$j]['title'],
                        'description' => $entMedia[$j]['description'],
                        'thumbnail_url' => $entMedia[$j]['thumbnail_url'],
                        'viewedAt' => $em->getRepository('PDOneBundle:Representative')->getMediaViewedAt(
                            $entEmail[$i]['id'],
                            $entMedia[$j]['id']
                        ) ? $em->getRepository('PDOneBundle:Representative')->getMediaViewedAt(
                            $entEmail[$i]['id'],
                            $entMedia[$j]['id']
                        ) : '0000-00-00 00:00:00',
                    ];
                }
            }

            # fix for https://bitbucket.org/magnific/mt020415-00-pdonereptool/issue/57/return-phone-number-with-rep_contact
            if (in_array($entEmail[$i]['messages_id'], $mid)) {
                $decode = json_decode($entEmail[$i]['data']);
                $message_text = $entEmail[$i]['message_text']."\nPlease call me at ".$decode->{'phone_number'};
            } else {
                $message_text = $entEmail[$i]['message_text'];
            }

            $response[] = [
                'id' => $entEmail[$i]['id'],
                'rep_id' => $entEmail[$i]['reps_id'],
                'target_id' => $entEmail[$i]['targets_id'],
                'target_displayname' => $entEmail[$i]['display_name'],
                'brands_id' => $entEmail[$i]['brands_id'],
                'brand_name' => $entEmail[$i]['name'],
                'messages_id' => $entEmail[$i]['messages_id'],
                'message_subject' => 'REP' == $entEmail[$i]['category'] ? sprintf(
                    $entEmail[$i]['message_subject'],
                    $entRep->getDisplayName()
                ) : sprintf($entEmail[$i]['message_subject'], $entEmail[$i]['display_name']),
                'message_text' => $message_text,
                'media' => $media,
                'category' => $entEmail[$i]['emails_category'],
                'createdAt' => $entEmail[$i]['createdAt'],
                'openedAt' => $em->getRepository('PDOneBundle:Representative')->getEmailOpenetAt(
                    $entEmail[$i]['id']
                ) ? $em->getRepository('PDOneBundle:Representative')->getEmailOpenetAt(
                    $entEmail[$i]['id']
                ) : '0000-00-00 00:00:00',
                'viewedAt' => $em->getRepository('PDOneBundle:Representative')->getEmailViewedAt(
                    $entEmail[$i]['id']
                ) ? $em->getRepository('PDOneBundle:Representative')->getEmailViewedAt(
                    $entEmail[$i]['id']
                ) : '0000-00-00 00:00:00',
            ];

            unset($media);
        }

        $view->setData($response)->setStatusCode(200);

        return $view;
    }

    /**
     * Set and upload avatar for reps.
     *
     * @param ParamFetcher $paramFetcher
     * @param Request      $request
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "Set and upload avatar for reps.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @RequestParam(name="rid", nullable=false, default=0, description="The ID of the representative")
     * @RequestParam(name="avatar", nullable=false, description="The avatar file")
     *
     * @return View
     */
    public function postRepsAvatarAction(ParamFetcher $paramFetcher, Request $request)
    {
        $view = View::create();
        $uploadPath = $this->container->getParameter('kernel.root_dir').'/../web/uploads/';

        if (null === $request->files->get('avatar')) {
            $view->setData(array())->setStatusCode(200);

            return $view;
        }

        $em = $this->getDoctrine()->getManager();
        $entReps = $em->getRepository('PDOneBundle:Representative')->find($paramFetcher->get('rid'));

        if (!$entReps) {
            $view->setData(array())->setStatusCode(200);

            return $view;
        }

        try {
            $uploadedAvatarFile = $request->files->get('avatar');

            /* @var $avatarFile \Symfony\Component\HttpFoundation\File\File */
            $avatarFile = $uploadedAvatarFile->move($uploadPath, $uploadedAvatarFile->getClientOriginalName());
            unset($uploadedAvatarFile);
        } catch (\ Exception $e) {
            $view->setData(
                array('error' => 'can not upload avatar file', 'message' => $e->getMessage())
            )->setStatusCode(200);

            return $view;
        }

        $credentials = new Credentials(
            $this->container->getParameter('amazon_s3_key'),
            $this->container->getParameter('amazon_s3_secret')
        );

        $client = S3Client::factory(
            array(
                'credentials' => $credentials,
            )
        );

        $avatarName = sprintf('%s.%s', $entReps->getVeevaRepId(), $avatarFile->getExtension());
        $resource = fopen($avatarFile->getRealPath(), 'r');

        try {
            $client->putObject(
                array(
                    'Bucket' => 'pdone/avatar',
                    'Key' => $avatarName,
                    'Body' => $resource,
                    'ACL' => 'public-read',
                )
            );

            // set the User Avatar
            $entReps->setAvatarUrl($client->getObjectUrl('pdone/avatar', $avatarName));
            $em->flush();
        } catch (S3Exception $e) {
            $view->setData(
                array('error' => 'can not upload avatar file', 'message' => $e->getMessage())
            )->setStatusCode(200);

            return $view;
        }

        unlink($avatarFile->getRealPath());

        $repsData = [
            'id' => $paramFetcher->get('rid'),
            'veeva_rep_id' => $entReps->getVeevaRepId(),
            'display_name' => $entReps->getDisplayName(),
            'avatar_url' => $entReps->getAvatarUrl(),
            'rep_type' => $entReps->getRepType(),
            'username' => $entReps->getUsername(),
            'first' => $entReps->getFirst(),
            'last' => $entReps->getLast(),
            'title' => $entReps->getTitle(),
            'phone' => $entReps->getPhone(),
            'email' => $entReps->getEmail(),
            'territory_id' => $entReps->getTerritory()->getTerritoryId(),
            'inactive' => $entReps->getInactive(),
            'total_contacts' => $em->getRepository('PDOneBundle:Representative')->getTotalContacts(
                $paramFetcher->get('rid')
            ),
            'total_shares' => $em->getRepository('PDOneBundle:Representative')->getTotalShares(
                $paramFetcher->get('rid')
            ),
            'totalViews' => $em->getRepository('PDOneBundle:Representative')->getTotalViews(
                $paramFetcher->get('rid')
            ),
            'lastLoginAt' => $entReps->getLastLoginAt(),
            'lastVeevaSyncAt' => $entReps->getUpdatedAt(),
            'createdAt' => $entReps->getCreatedAt(),
            'updatedAt' => $entReps->getUpdatedAt(),
        ];

        $view->setData($repsData)->setStatusCode(200);

        return $view;
    }
}

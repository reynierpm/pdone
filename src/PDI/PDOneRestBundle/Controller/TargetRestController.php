<?php

/**
 * RestAPI =>       Target.
 *
 * @author        Reynier Perez <reynier@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneRestBundle\Controller;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use PDI\PDOneBundle\Entity\Email;
use PDI\PDOneBundle\Entity\Event;

class TargetRestController extends FOSRestController
{
    /**
     * Update details for target.
     *
     * @param ParamFetcher $paramFetcher
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "Update details for reps.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @RequestParam(name="tid", nullable=false, strict=true, description="The ID of the target")
     * @RequestParam(name="email", nullable=false, strict=true, description="The email value to update")
     *
     * @return View
     */
    public function putTargetsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $view = View::create();

        $entTarget = $em->getRepository('PDOneBundle:Target')->find($paramFetcher->get('tid'));

        if (!$entTarget) {
            $view->setData(array())->setStatusCode(200);

            return $view;
        }

        $entTarget->setEmail($paramFetcher->get('email'));
        $em->flush();

        $respTarget = [
            'id' => $entTarget->getVeevaAccountId(),
            'veeva_account_id' => $entTarget->getVeevaAccountId(),
            'veeva_timestamp' => $entTarget->getVeevaTimestamp(),
            'display_name' => $entTarget->getDisplayName(),
            'avatar_url' => $entTarget->getAvatarUrl(),
            'title' => $entTarget->getTitle(),
            'first' => $entTarget->getFirst(),
            'last' => $entTarget->getLast(),
            'suffix' => $entTarget->getSuffix(),
            'address1' => $entTarget->getAddress1(),
            'address2' => $entTarget->getAddress2(),
            'city' => $entTarget->getCity(),
            'state' => $entTarget->getState(),
            'zip' => $entTarget->getZip(),
            'phone' => $entTarget->getPhone(),
            'fax' => $entTarget->getFax(),
            'email' => $entTarget->getEmail(),
            'state_licensed' => $entTarget->getStateLicensed(),
            'state_licensed_id' => $entTarget->getStateLicensedId(),
            'target_type' => $entTarget->getTargetType(),
            'npi' => $entTarget->getNpi(),
            'practioner_id' => $entTarget->getPractionerId(),
            'inactive' => $entTarget->getInactive(),
            'last_sync_at' => $entTarget->getUpdatedAt(),
            'createdAt' => $entTarget->getCreatedAt(),
            'updatedAt' => $entTarget->getUpdatedAt(),
        ];

        $view->setData($respTarget)->setStatusCode(200);

        return $view;
    }

    /**
     * Get target activity.
     *
     * @param ParamFetcher $paramFetcher
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "Get target activity.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @QueryParam(name="tid", nullable=false, strict=true, description="The ID of the target")
     *
     * @return View
     */
    public function getTargetsActivityAction(ParamFetcher $paramFetcher)
    {
        $view = View::create();

        $em = $this->getDoctrine()->getManager();
        $target_activity = $share_details = [];

        $entReps = $em->getRepository('PDOneBundle:Representative')->findRepByTarget($paramFetcher->get('tid'));
        $territoryId = $em->getRepository('PDOneBundle:Territory')->getTerritoryByTarget($paramFetcher->get('tid'));

        if ($entReps === null) {
            $repsData = [
                'share_details' => [],
                'target_activity' => [],
            ];

            $view->setData($repsData)->setStatusCode(200);

            return $view;
        }

        $entShareDetails = $em->getRepository('PDOneBundle:Representative')->getAllMedia($territoryId);
        for ($i = 0; $i < count($entShareDetails); ++$i) {
            $share_details[] = [
                'media_id' => $entShareDetails[$i]['id'],
                'title' => $entShareDetails[$i]['title'],
                'description' => $entShareDetails[$i]['description'],
                'thumbnail_url' => $entShareDetails[$i]['thumbnail_url'],
                'total_shares' => $em->getRepository('PDOneBundle:Target')->getTotalShares(
                    $entReps['rep_id'],
                    $paramFetcher->get('tid'),
                    $entShareDetails[$i]['id']
                ) ? $em->getRepository('PDOneBundle:Target')->getTotalShares(
                    $entReps['rep_id'],
                    $paramFetcher->get('tid'),
                    $entShareDetails[$i]['id']
                ) : '0',
                'total_views' => $em->getRepository('PDOneBundle:Target')->getTotalViews(
                    $entReps['rep_id'],
                    $paramFetcher->get('tid'),
                    $entShareDetails[$i]['id']
                ) ? $em->getRepository('PDOneBundle:Target')->getTotalViews(
                    $entReps['rep_id'],
                    $paramFetcher->get('tid'),
                    $entShareDetails[$i]['id']
                ) : '0',
                'last_viewed' => $em->getRepository('PDOneBundle:Target')->getLastViewed(
                    $entReps['rep_id'],
                    $paramFetcher->get('tid'),
                    $entShareDetails[$i]['id']
                ) ? $em->getRepository('PDOneBundle:Target')->getLastViewed(
                    $entReps['rep_id'],
                    $paramFetcher->get('tid'),
                    $entShareDetails[$i]['id']
                ) : '0000-00-00 00:00:00',
                'last_shared' => $em->getRepository('PDOneBundle:Target')->getLastShared(
                    $entReps['rep_id'],
                    $paramFetcher->get('tid'),
                    $entShareDetails[$i]['id']
                ) ? $em->getRepository('PDOneBundle:Target')->getLastShared(
                    $entReps['rep_id'],
                    $paramFetcher->get('tid'),
                    $entShareDetails[$i]['id']
                ) : '0000-00-00 00:00:00',
            ];
        }

        $entTargetActivity = $em->getRepository('PDOneBundle:Target')->getTargetsActivity(
            $entReps['rep_id'],
            $paramFetcher->get('tid')
        );

        for ($i = 0; $i < count($entTargetActivity); ++$i) {
            switch ($entTargetActivity[$i]['event_type']) {
                case 'email_sent':
                    $title = 'You contacted '.$entTargetActivity[$i]['display_name'].'.';
                    $description = str_replace(
                        '%s',
                        $entReps['display_name'],
                        $entTargetActivity[$i]['message_subject']
                    );
                    $thumbnail = 'https://pdone.s3.amazonaws.com/activity/email_sent.png';
                    break;

                case 'email_view':
                    $title = $entTargetActivity[$i]['display_name'].' viewed your message.';
                    $description = str_replace(
                        '%s',
                        $entReps['display_name'],
                        $entTargetActivity[$i]['message_subject']
                    );
                    $thumbnail = 'https://pdone.s3.amazonaws.com/activity/email_view.png';
                    break;

                case 'page_view':
                    $title = $entTargetActivity[$i]['display_name'].' opened your email.';
                    $description = str_replace(
                        '%s',
                        $entTargetActivity[$i]['display_name'],
                        $entTargetActivity[$i]['message_text']
                    );
                    $thumbnail = 'https://pdone.s3.amazonaws.com/activity/page_view.png';
                    break;

                case 'media_view':
                    $title = $entTargetActivity[$i]['display_name'].' viewed '.$entTargetActivity[$i]['title'].'.';
                    $description = $entTargetActivity[$i]['description'];
                    $thumbnail = 'https://pdone.s3.amazonaws.com/activity/media_view.png';
                    break;

                case 'rep_contact':
                    $title = $entTargetActivity[$i]['display_name'].' contacted you.';

                    # fix for https://bitbucket.org/magnific/mt020415-00-pdonereptool/issue/57/return-phone-number-with-rep_contact
                    if (in_array($entTargetActivity[$i]['mid'], $this->container->getParameter('mediaIds'))) {
                        $description = str_replace(
                            '%s',
                            $entTargetActivity[$i]['display_name'],
                            $entTargetActivity[$i]['message_text']
                        );

                        if ($entTargetActivity[$i]['data'] != null || $entTargetActivity[$i]['data'] != '') {
                            $decode = json_decode($entTargetActivity[$i]['data']);
                            $description .= "\nPlease call me at $decode->{'phone_number'}.";
                        }
                    } else {
                        $description = str_replace(
                            '%s',
                            $entTargetActivity[$i]['display_name'],
                            $entTargetActivity[$i]['message_text']
                        );
                    }

                    $thumbnail = 'https://pdone.s3.amazonaws.com/activity/rep_contact.png';
                    break;
            }

            $target_activity[] = [
                'title' => $title,
                'description' => $description,
                'thumbnail' => $thumbnail,
                'timestamp' => $entTargetActivity[$i]['createdAt'],
                'event_type' => $entTargetActivity[$i]['event_type'],
            ];
        }

        $repsData = [
            'share_details' => $share_details,
            'target_activity' => $target_activity,
        ];

        $view->setData($repsData)->setStatusCode(200);

        return $view;
    }

    /**
     * Create a new email for target.
     *
     * @param ParamFetcher $paramFetcher
     * @ApiDoc(
     *      resource = true,
     *      https = true,
     *      description = "Create a new email for target.",
     *      statusCodes = {
     *          200 = "Returned when successful",
     *          400 = "Returned when errors"
     *      }
     * )
     * @RequestParam(name="tid", nullable=false, strict=true, description="The ID of the target")
     * @RequestParam(name="bid", nullable=false, strict=true, requirements="\d+", description="The ID of the brand")
     * @RequestParam(name="mid", nullable=false, strict=true, requirements="\d+", description="The ID of the message")
     * @RequestParam(name="medias", nullable=false, description="The IDs of the medias")
     *
     * @return View
     */
    public function postEmailsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();
        $view = View::create();

        if ($this->container->getParameter('kernel.environment') == 'dev') {
            // set DEV vars
            $shareUrl = 'http://share.qa.pdone.com/app_dev.php';
            $apiUrl = 'http://api.qa.pdone.com/app_dev.php';
        } else {
            // set PROD vars
            $shareUrl = 'http://share.pdone.com';
            $apiUrl = 'http://api.pdone.com';
        }

        $entTarget = $em->getRepository('PDOneBundle:Target')->find($paramFetcher->get('tid'));
        $entBrand = $em->getRepository('PDOneBundle:Brand')->find($paramFetcher->get('bid'));
        $entMessage = $em->getRepository('PDOneBundle:Message')->find($paramFetcher->get('mid'));
        $entReps = $em->getRepository('PDOneBundle:Representative')->findRepByTarget($paramFetcher->get('tid'));
        $arrMedias = explode(',', $paramFetcher->get('medias'));

        if (!$entTarget || !$entBrand || !$entMessage || !$arrMedias || $entReps === null) {
            $view->setData(array())->setStatusCode(200);

            return $view;
        }

        $entEmail = new Email();
        $entEmail->setRep(
            $em->getReference('PDOneBundle:Representative', $entReps['rep_id'])
        );
        $entEmail->setTarget($entTarget);
        $entEmail->setEmailsCategory('sent');
        $entEmail->addEmailsMessageXref($entMessage);
        # fixed issue when SQL mode is set to STRICT - see more at http://stackoverflow.com/a/33425397/719427
        $entEmail->setViewedAt(null);

        for ($i = 0; $i < count($arrMedias); ++$i) {
            $entEmail->addEmailsMediaXref(
                $em->getReference('PDOneBundle:Media', $arrMedias[$i])
            );
        }

        $entEmail->setDeliveredAt(new \DateTime());
        $em->persist($entEmail);
        $em->flush();

        // Track email_sent event
        $trackEmail = new Event();
        $trackEmail->setRepId($entReps['rep_id']);
        $trackEmail->setBrandId($entBrand->getId());
        $trackEmail->setTargetId($entTarget->getVeevaAccountId());
        $trackEmail->setEmailId($entEmail->getId());
        $trackEmail->setEventType('email_sent');

        $em->persist($trackEmail);

        try {
            $em->flush();
        } catch (\PDOException $e) {
            $view->setData(
                array(
                    'error' => $e->getMessage(),
                )
            )->setStatusCode(400);

            return $view;
        }

        $brand_page = $shareUrl.'?email='.$this->get('nzo_url_encryptor')->encrypt(
                $entEmail->getId()
            ).'&modal=1';
        $medias = [];

        for ($i = 0; $i < count($arrMedias); ++$i) {
            $entMedia = $em->getRepository('PDOneBundle:Media')->find($arrMedias[$i]);
            $medias[] = [
                'media_id' => (string) $entMedia->getId(),
                'media_title' => (int) $entMedia->getUseHtml() === 1 ? $entMedia->getTitleHtml() : $entMedia->getTitle(),
                'description' => (int) $entMedia->getUseHtml() === 1 ? $entMedia->getDescriptionHtml() : $entMedia->getDescription(),
                'thumbnail_url' => $entMedia->getThumbnailUrl(),
                'viewedAt' => $em->getRepository('PDOneBundle:Representative')->getMediaViewedAt(
                    $entEmail->getId(),
                    $arrMedias[$i]
                ) ? $em->getRepository('PDOneBundle:Representative')->getMediaViewedAt(
                    $entEmail->getId(),
                    $arrMedias[$i]
                ) : '0000-00-00 00:00:00',
                'view_url' => $shareUrl.'/?email='.$this->get(
                        'nzo_url_encryptor'
                    )->encrypt($entEmail->getId()).'&media='.$this->get('nzo_url_encryptor')->encrypt(
                        $entMedia->getId()
                    ),
            ];

            // Track email_sent event
            $trackEmail = new Event();
            $trackEmail->setRepId($entReps['rep_id']);
            $trackEmail->setBrandId($entBrand->getId());
            $trackEmail->setTargetId($entTarget->getVeevaAccountId());
            $trackEmail->setEmailId($entEmail->getId());
            $trackEmail->setMediaId($entMedia->getId());
            $trackEmail->setEventType('media_share');

            $em->persist($trackEmail);
        }

        try {
            $em->flush();
        } catch (\PDOException $e) {
            $view->setData(
                array(
                    'error' => $e->getMessage(),
                )
            )->setStatusCode(400);

            return $view;
        }

        // send email through Mandrill API
        $mandrill = new \Mandrill(
            $this->container->getParameter('mandrill_api_key')
        );

        try {
            $template_content = [];
            $template_name = $entMessage->getEmailTemplate();
            $messageSubject = in_array($entMessage->getId(), $this->container->getParameter('messageIds')) ? sprintf($entMessage->getMessageSubject(), $entReps['display_name']) : $entMessage->getMessageSubject();
            $message = array(
                'html' => null,
                'text' => null,
                'subject' => $messageSubject,
                'from_email' => 'donotreply@pdone.com',
                'from_name' => $entReps['display_name'],
                'to' => array(
                    array(
                        'email' => $entTarget->getEmail(),
                        'name' => ucfirst(strtolower($entTarget->getFirst())).' '.ucfirst(
                                strtolower($entTarget->getLast())
                            ),
                        'type' => 'to',
                    ),
                ),
                'important' => null,
                'track_opens' => true,
                'track_clicks' => true,
                'url_strip_qs' => null,
                'view_content_link' => null,
                'tracking_domain' => null,
                'signing_domain' => null,
                'return_path_domain' => null,
                'merge' => true,
                'inline_css' => true,
                'merge_language' => 'handlebars',
                'metadata' => array('eid' => $this->get('nzo_url_encryptor')->encrypt($entEmail->getId())),
                'global_merge_vars' => array(
                    array(
                        'name' => 'rep_avatar_url',
                        'content' => $entReps['avatar_url'],
                    ),
                    array(
                        'name' => 'unsub_url',
                        'content' => $shareUrl.'/?email='.$this->get('nzo_url_encryptor')->encrypt($entEmail->getId()).'&action=UNSUB',

                    ),
                    array(
                        'name' => 'hcp_name',
                        'content' => ucfirst(strtolower($entTarget->getFirst())).' '.ucfirst(
                                strtolower($entTarget->getLast())
                            ),
                    ),
                    array(
                        'name' => 'brand_page',
                        'content' => $brand_page,
                    ),
                    array(
                        'name' => 'rep_name',
                        'content' => $entReps['display_name'],
                    ),
                    array(
                        'name' => 'media',
                        'content' => $medias,
                    ),
                    array(
                        'name' => 'track_open',
                        'content' => $apiUrl.'/api/v1/tracking/email?_format=json&eid='.$this->get(
                                'nzo_url_encryptor'
                            )->encrypt($entEmail->getId()),
                    ),
                ),
            );

            $async = false;
            $ip_pool = 'Main Pool';
            $send_at = null;
            $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);

            $entEmail->setDeliveredAt(new \DateTime());
            $em->flush();
        } catch (Mandrill_Error $e) {
            $view->setData(
                array(
                    'error' => 'A mandrill error occurred: '.get_class($e).' - '.$e->getMessage(),
                )
            )->setStatusCode(400);

            return $view;
        }

        $respEmail = [
            'id' => (string) $entEmail->getId(),
            'targets_id' => $paramFetcher->get('tid'),
            'target_displayname' => $entTarget->getDisplayName(),
            'brands_id' => $paramFetcher->get('bid'),
            'brand_name' => $entBrand->getName(),
            'messages_id' => $paramFetcher->get('mid'),
            'message_subject' => $messageSubject,
            'message_text' => $entMessage->getMessageText(),
            'media' => $medias,
            'category' => $entEmail->getEmailsCategory(),
            'createdAt' => $entEmail->getCreatedAt()->format('Y-m-d H:i:s'),
        ];

        $view->setData($respEmail)->setStatusCode(200);

        return $view;
    }
}

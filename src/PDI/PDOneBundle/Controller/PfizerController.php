<?php

namespace PDI\PDOneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class PfizerController extends Controller
{
    /**
     * @param Request $request
     *
     * @return array
     * @Route("/pfizer", name="pfizer")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $message_code = $request->query->get('message_code');
        $hcp_name = $request->query->get('hcp_name');
        $rep_id = $request->query->get('rep_id');
        $rep_name = '';
        $msg = '';
        $title = '';
        switch ($rep_id) {
            case 134:
            $rep_name = 'Daniel Pugsley';
            break;
            case 188:
            $rep_name = 'George Reisdorf';
            break;
            case 54:
            $rep_name = 'Brian Buchalski';
            break;
            default:
                return $this->render('PDOneBundle:Pfizer:error404.html.twig');
        }
        if ($message_code && $hcp_name && $rep_id) {
            switch ($message_code) {
                case 'QXR673210-05':
                $title = 'Sorry our call was interrupted';
                $msg = "SORRY WE COULD NOT COMPLETE OUR CALL<br /><br />
                Hello $hcp_name,<br /><br />
                It appears that we are having difficulties connecting via Adobe Connect and/or telephone.<br /><br />
                Please <a href='https://www.pfizerpro.com/product/quillivant-xr/adhd/support/request-a-virtual-sales-rep'>click here</a> to schedule another call. As a reminder, you’ll need to access your computer during the call in order to view the presentation via Adobe Connect.<br /><br />
                In the meantime, please visit <a href='http://www.quillivantxrhcp.com'>QuillivantXRhcp.com</a> to find information about Quillivant XR.<br /><br />
                I look forward to speaking with you!";

                break;
                case 'QXR673210-06':
                $title = 'Get ready for your online presentation';
                $msg = "HERE IS YOUR PRESENTATION LINK FOR OUR CALL<br /><br />
                Hello $hcp_name,<br />
                Thank you for your interest in Quillivant XR.<br /><br />
                This is your Quillivant XR sales representative. Per your request, I will call the number you provided to guide you through our Adobe Connect presentation about Quillivant XR.<br /><br />";
                switch ($rep_id) {
                    case 134:
                    $msg .= 'As a reminder, you will need to access your computer during our call, so please <a href="http://pdi.adobeconnect.com/pugsley">click here</a> to view the presentation. <br /><br />';
                    break;
                    case 188:
                    $msg .= 'As a reminder, you will need to access your computer during our call, so please <a href="http://pdi.adobeconnect.com/reisdorf">click here</a> to view the presentation. <br /><br />';
                    break;
                    case 54:
                    $msg .= 'As a reminder, you will need to access your computer during our call, so please <a href="http://pdi.adobeconnect.com/buchalski">click here</a> to view the presentation. <br /><br />';
                    break;
                    default:
                    $msg .= 'As a reminder, you will need to access your computer during our call.<br /><br />';
                    break;
                }
                $msg .= 'I look forward to speaking with you!';
                break;
                case 'QXR673210-07':
                $title = 'Reschedule your call';
                $msg = "SORRY WE MISSED YOU<br /><br />
                Hello $hcp_name,<br /><br />
                I am a Sales Representative for Quillivant XR. As you requested, I tried to call you to provide information about Quillivant XR, but was unable to reach you.<br /><br />
                Would you like to set up another call? You can <a href='https://www.pfizerpro.com/product/quillivant-xr/adhd/support/request-a-virtual-sales-rep'>click here</a> to reschedule.<br /><br />
                In the meantime, please visit visit <a href='http://www.quillivantxrhcp.com'>QuillivantXRhcp.com</a> to find information about Quillivant XR.<br /><br />
                Thank you again for your interest in Quillivant XR.";
                break;
                case 'QXR673210-08':
                $title = 'Thank you for requesting a virtual sales rep';
                $msg = "THANK YOU FOR REQUESTING A VIRTUAL SALES REPRESENTATIVE<br /><br />
                Hello $hcp_name,<br /><br />
                Thank you for your interest in Quillivant XR. I will call you at the number provided on the day of the week you requested.<br /><br />
                As a reminder, you will need to access your computer during the call in order to view the presentation via Adobe Connect.<br /><br />
                I look forward to speaking with you!";
                break;
                case 'QXR673210-09':
                $title = 'Thank you for your time';
                $msg = "THANK YOU FOR YOUR TIME<br /><br />
                Hello $hcp_name,<br /><br />
                Thank you for taking the time to speak with me. I hope that our call was informative.<br /><br />
                To learn more about Quillivant XR and resources such as the Pharmacy Locator and Home Delivery, visit <a href='http://www.quillivantxrhcp.com'>QuillivantXRhcp.com</a>.<br /><br />
                Sincerely,";
                break;
                default:
                    return $this->render('PDOneBundle:Pfizer:error404.html.twig');
            }
        } else {
            return $this->render('PDOneBundle:Pfizer:error404.html.twig');
        }

        return array(
            'title' => $title,
            'rep_name' => $rep_name,
            'msg' => $msg,
        );
    }
}

<?php

namespace PDI\PDOneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

class DashboardController extends Controller
{
    /**
     * @Route("/dashboard/get-report", name="get-report")
     * @Method("GET")
     */
    public function getReportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $response = array();
        $reportList = array();
        $start_date = date('Y/m/d', strtotime('first day of previous Month'));
        $start_date_monday = date($start_date, strtotime('last Monday'));
        $length = date('W') - date('W', strtotime($start_date_monday));
        $brand_id = $request->get('brand');
        $territory_id = $request->get('territory');
        $media_id = $request->get('media');
        for ($i = 0; $i <= $length; ++$i) {
            $repapp = array();
            $range = $this->weekRange($start_date_monday);
            // Week of June 8th 2015
            $repapp[] = '<strong>'.date('W', strtotime($start_date_monday)).'-'.date('\W\e\e\k \of F jS Y', strtotime($start_date_monday)).'</strong>';
            $repapp[] = $em->getRepository('PDOneBundle:Representative')->getRepsInField($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Reps in Field
            $repapp[] = $em->getRepository('PDOneBundle:Representative')->getRepsWhoShared($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Reps who shared
            $emailSend = $em->getRepository('PDOneBundle:Representative')->getTotalEmailsSendByRange($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);
            $repapp[] = $emailSend;//Emails Sent
            $hcpShare = $em->getRepository('PDOneBundle:Representative')->getTotalTargetSharesByRange($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);
            $repapp[] = ($hcpShare === null) ? 0 : $hcpShare;//HCPs Shared with
            $repapp[] = $em->getRepository('PDOneBundle:Representative')->getTotalMediaSharesByRange($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Content Shares
            $emailOpen = $em->getRepository('PDOneBundle:Representative')->getTotalEmailsViewByRange($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);
            $repapp[] = $emailOpen;//Email Opens
            $repapp[] = ($emailSend > 0) ? round(($emailOpen / $emailSend) * 100, 2).' %' : '0 % ';//Email Open Rate
            $emailClickThru = $em->getRepository('PDOneBundle:Representative')->getTotalPagesViewByRange($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);
            $repapp[] = $emailClickThru;//Email Click-Thru
            $repapp[] = ($emailOpen > 0) ? round(($emailOpen / $emailOpen) * 100, 2).' %' : '0 % ';//CTR
            $repapp[] = $em->getRepository('PDOneBundle:Representative')->getUniqueTargetView($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Unique HCP Views
            $reportList[] = $repapp;
            $dt = strtotime($start_date_monday);
            $start_date_monday = date('Y-m-d', strtotime('next monday', $dt));
        }
        $response['data'] = $reportList;

        return new JsonResponse($response);
    }

    /**
     * @Route("/dashboard/get-report-export", name="get-report-export")
     * @Template()
     */
    public function getReportExportAction()
    {
        $container = $this->container;
        $reportList = array();
        $response = new StreamedResponse(function () use ($container) {

            $em = $container->get('doctrine')->getManager();
            $handle = fopen('php://output', 'r+');
            $start_date = date('Y/m/d', strtotime('first day of previous Month'));
            $start_date_monday = date($start_date, strtotime('last Monday'));
            $length = date('W') - date('W', strtotime($start_date_monday));

            $repapp = array();
            for ($i = 0; $i <= $length; ++$i) {
                $range = $this->weekRange($start_date_monday);
                $repapp['week'][''] = '';
                $repapp['week'][] = 'Week of '.$range['label'];
                $repapp['rep']['Reps in Field'] = 'Reps in Field';
                $repapp['rep'][] = $em->getRepository('PDOneBundle:Representative')->getRepsInField($range['startD'], $range['endD']);//Reps in Field
                $repapp['shared']['Reps Who Shared'] = 'Reps Who Shared';
                $repapp['shared'][] = $em->getRepository('PDOneBundle:Representative')->getRepsWhoShared($range['startD'], $range['endD']);//Reps who shared
                $emailSend = $em->getRepository('PDOneBundle:Representative')->getTotalEmailsSendByRange($range['startD'], $range['endD']);
                $repapp['ems']['Emails Sent'] = 'Emails Sent';
                $repapp['ems'][] = $emailSend;//Emails Sent
                $hcpShare = $em->getRepository('PDOneBundle:Representative')->getTotalTargetSharesByRange($range['startD'], $range['endD']);
                $repapp['hcp']['HCPs Shared With'] = 'HCPs Shared With';
                $repapp['hcp'][] = ($hcpShare === null) ? 0 : $hcpShare;//HCPs Shared with
                $repapp['consh']['Content Shares'] = 'Content Shares';
                $repapp['consh'][] = $em->getRepository('PDOneBundle:Representative')->getTotalMediaSharesByRange($range['startD'], $range['endD']);//Content Shares
                $emailOpen = $em->getRepository('PDOneBundle:Representative')->getTotalEmailsViewByRange($range['startD'], $range['endD']);
                $repapp['emo']['Email Opens'] = 'Email Opens';
                $repapp['emo'][] = $emailOpen;//Email Opens
                $repapp['opr']['Email Open Rate'] = 'Email Open Rate';
                $repapp['opr'][] = ($emailSend > 0) ? round(($emailOpen / $emailSend) * 100, 2).' %' : '0 % ';//Email Open Rate
                $emailClickThru = $em->getRepository('PDOneBundle:Representative')->getTotalPagesViewByRange($range['startD'], $range['endD']);
                $repapp['emc']['Email Click-Thru s'] = 'Email Click-Thru s';
                $repapp['emc'][] = $emailClickThru;//Email Click-Thru
                $repapp['ctr']['CTR'] = 'CTR';
                $repapp['ctr'][] = ($emailOpen > 0) ? round(($emailOpen / $emailOpen) * 100, 2).' %' : '0 % ';//CTR
                $repapp['uhcp']['Unique HCP Views'] = 'Unique HCP Views';
                $repapp['uhcp'][] = $em->getRepository('PDOneBundle:Representative')->getUniqueTargetView($range['startD'], $range['endD']);//Unique HCP Views

                $dt = strtotime($start_date_monday);
                $start_date_monday = date('Y-m-d', strtotime('next monday', $dt));
            }

            foreach ($repapp as $row) {
                fputcsv($handle, $row, ',');
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }

    /**
     * @Route("/dashboard/get-metric-one", name="get-metric-one")
     * @Method("GET")
     */
    public function getMetricOneAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $response = array();
        $start_date = date('Y/m/d', strtotime('first day of previous Month'));
        $start_date_monday = date($start_date, strtotime('last monday'));
        $length = date('W') - date('W', strtotime($start_date));
        $brand_id = $request->get('brand');
        $territory_id = $request->get('territory');
        $media_id = $request->get('media');
        for ($i = 0; $i <= $length; ++$i) {
            $repapp = array();
            $range = $this->weekRange($start_date_monday);
            $repapp['date'] = date('\W\e\e\k \of F jS Y', strtotime($start_date_monday));
            $repapp['reps'] = $em->getRepository('PDOneBundle:Representative')->getRepsInField($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Reps in Field
            $repapp['repsShared'] = $em->getRepository('PDOneBundle:Representative')->getRepsWhoShared($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Reps who shared
            $response[] = $repapp;
            $dt = strtotime($start_date_monday);
            $start_date_monday = date('Y-m-d', strtotime('next monday', $dt));
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/dashboard/get-metric-two", name="get-metric-two")
     * @Method("GET")
     */
    public function getMetricTwoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $response = array();
        $start_date = date('Y/m/d', strtotime('first day of previous Month'));
        $start_date_monday = date($start_date, strtotime('last monday'));
        $length = date('W') - date('W', strtotime($start_date));
        $brand_id = $request->get('brand');
        $territory_id = $request->get('territory');
        $media_id = $request->get('media');
        for ($i = 0; $i <= $length; ++$i) {
            $repapp = array();
            $range = $this->weekRange($start_date_monday);
            $repapp['x'] = date('Y-m-d', strtotime($start_date_monday));
            $repapp['sent'] = $em->getRepository('PDOneBundle:Representative')->getTotalEmailsSendByRange($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Emails Sent
            $repapp['view'] = $em->getRepository('PDOneBundle:Representative')->getTotalEmailsViewByRange($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Emails view
            $repapp['page'] = $em->getRepository('PDOneBundle:Representative')->getTotalPagesViewByRange($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Pages view
            $dt = strtotime($start_date_monday);
            $start_date_monday = date('Y-m-d', strtotime('next monday', $dt));
            $response[] = $repapp;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/dashboard/get-metric-three", name="get-metric-three")
     * @Method("GET")
     */
    public function getMetricThreeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $response = array();
        $start_date = date('Y/m/d', strtotime('first day of previous Month'));
        $start_date_monday = date($start_date, strtotime('last monday'));
        $length = date('W') - date('W', strtotime($start_date));
        $brand_id = $request->get('brand');
        $territory_id = $request->get('territory');
        $media_id = $request->get('media');
        for ($i = 0; $i <= $length; ++$i) {
            $repapp = array();
            $range = $this->weekRange($start_date_monday);
            $repapp['x'] = date('Y-m-d', strtotime($start_date_monday));
            $repapp['shares'] = $em->getRepository('PDOneBundle:Representative')->getTotalMediaSharesByRange($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Media Shares
            $repapp['views'] = $em->getRepository('PDOneBundle:Representative')->getTotalMediaViewByRange($range['startD'], $range['endD'], $brand_id, $territory_id, $media_id);//Media Views
            $dt = strtotime($start_date_monday);
            $start_date_monday = date('Y-m-d', strtotime('next monday', $dt));
            $response[] = $repapp;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/dashboard/get-raw-data", name="get-raw-data")
     * @Template()
     */
    public function getRawDataAction(Request $request)
    {
        $container = $this->container;
        $response = new StreamedResponse(function () use ($container) {
            $em = $container->get('doctrine')->getManager();
            $handle = fopen('php://output', 'r+');
            fputcsv($handle, array('id', 'event_type', 'rep_id', 'target_id', 'brandname', 'media_code', 'message_code', 'territories_id', 'createdAt'), ',');
            $results = $em->getRepository('PDOneBundle:Event')->getExportQuery();
            while ($row = $results->fetch()) {
                fputcsv($handle, array($row['id'], $row['event_type'], $row['rep_id'], $row['target_id'], $row['brandname'], $row['media_code'], $row['message_code'], $row['territories_id'], $row['createdAt']), ',');
            }
            fclose($handle);
        });
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }

    /**
     * @Route("/dashboard/brands", name="get-brands")
     * @Method("GET")
     */
    public function getBrandsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('PDOneBundle:Brand')->findAll();

        if ($request->isXmlHttpRequest()) {
            $response = array();

            foreach ($entities as $brand) {
                $dataResponse = array();
                $dataResponse['id'] = $brand->getId();
                $dataResponse['text'] = $brand->getName();
                $response[] = $dataResponse;
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/dashboard/territories", name="get-territories")
     * @Method("GET")
     */
    public function getTerritoriesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('PDOneBundle:Territory')->getTerritoryByFilter($request->get('filter'));

        if ($request->isXmlHttpRequest()) {
            $response = array();

            foreach ($entities as $territory) {
                $dataResponse = array();
                $dataResponse['id'] = $territory->getTerritoryId();
                $dataResponse['text'] = $territory->getName();
                $response[] = $dataResponse;
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/dashboard/medias", name="get-medias")
     * @Method("GET")
     */
    public function getMediasAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('PDOneBundle:Media')->findAll();

        if ($request->isXmlHttpRequest()) {
            $response = array();

            foreach ($entities as $media) {
                $dataResponse = array();
                $dataResponse['id'] = $media->getId();
                $dataResponse['text'] = $media->getTitle();
                $response[] = $dataResponse;
            }
        }

        return new JsonResponse($response);
    }

    private function weekRange($date)
    {
        $ts = strtotime($date);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last friday', $ts);
        $response = array(
            'label' => date('m/d', $start).'-'.date('m/d', strtotime('next friday', $start)),
            'startD' => date('Y/m/d', $start),
            'endD' => date('Y/m/d', strtotime('next friday', $start)),
        );

        return $response;
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     *
     * @return array
     */
    private function getFormErrors(\Symfony\Component\Form\Form $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }
}

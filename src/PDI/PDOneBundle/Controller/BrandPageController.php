<?php

namespace PDI\PDOneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use PDI\PDOneBundle\Entity;

class BrandPageController extends Controller
{
    /**
     * @param Request $request
     *
     * @return array
     * @Route("/", name="brand")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $modal = null;
        $template = 'index';

        if ($request->query->get('email')) {
            $email_id = $this->get('nzo_url_encryptor')->decrypt($request->query->get('email'));
            $entityMail = $em->getRepository('PDOneBundle:Email')->find($email_id);
            $modal = $request->query->get('modal');

            if ($entityMail) {
                $brand_id = 0;
                $entityBrand = null;
                $entityCompany = null;
                $entityRep = $entityMail->getRep();
                $entityTarget = $entityMail->getTarget();
                $rep_id = $entityRep->getVeevaRepId();
                $target_id = $entityTarget->getVeevaAccountId();
                $mediasArr = $em->getRepository('PDOneBundle:Target')->getTargetMedias($target_id);

                if (count($mediasArr) > 0) {
                    $entityBrand = $em->getRepository('PDOneBundle:Brand')->find($mediasArr[0]['brands_id']);
                    $entityCompany = $entityBrand->getCompany();
                    $template = $entityBrand->getTemplateName();
                    $brand_id = $entityBrand->getId();
                } else {
                    throw new NotFoundHttpException('The page no exists');
                }

                $track = $em->getRepository('PDOneBundle:Event')->findOneBy(
                    array(
                        'event_type' => 'page_view',
                        'rep_id' => $rep_id,
                        'target_id' => $target_id,
                        'brand_id' => $brand_id,
                        'email_id' => $email_id,
                    ),
                    array('createdAt' => 'DESC')
                );

                $now = new \DateTime();

                if ($track === null) {
                    $event = $this->saveEvent('page_view', $rep_id, $target_id, $brand_id, $email_id, null, $now);
                    $em->persist($event);
                    $em->flush();
                }

                if ($request->query->get('action')) {
                    if ($request->query->get('action') === 'UNSUB') {
                        return $this->forward(
                                'PDOneBundle:BrandPage:unsub',
                                array(
                                    'email_id' => $this->get('nzo_url_encryptor')->encrypt($email_id), //$email_id,
                                    'target_id' => $this->get('nzo_url_encryptor')->encrypt($target_id), //$target_id,
                                    'template' => $template,
                                )
                        );
                    }
                }

                if ($request->query->get('media')) {
                    $media_id = $this->get('nzo_url_encryptor')->decrypt($request->query->get('media'));
                    $entityMedia = $em->getRepository('PDOneBundle:Media')->find($media_id);

                    if ($entityMedia) {
                        $track = $em->getRepository('PDOneBundle:Event')->findOneBy(
                            array(
                                'event_type' => 'media_view',
                                'rep_id' => $rep_id,
                                'target_id' => $target_id,
                                'brand_id' => $brand_id,
                                'email_id' => $email_id,
                                'media_id' => $media_id,
                            ),
                            array('createdAt' => 'DESC')
                        );

                        if ($track === null) {
                            $event = $this->saveEvent(
                                'media_view',
                                $rep_id,
                                $target_id,
                                $brand_id,
                                $email_id,
                                $media_id,
                                $now
                            );
                            $em->persist($event);
                            $em->flush();
                        }

                        return $this->forward(
                            'PDOneBundle:BrandPage:show',
                            array(
                                'media_id' => $this->get('nzo_url_encryptor')->encrypt($media_id), //$media_id,
                                'rep_id' => $this->get('nzo_url_encryptor')->encrypt($rep_id), //$rep_id,
                                'email_id' => $this->get('nzo_url_encryptor')->encrypt($email_id), //$email_id,
                            )
                        );
                    } else {
                        throw new NotFoundHttpException('The page no exists');
                    }
                } else {
                    $medias = $mediasArr;
                }
            } else {
                throw new NotFoundHttpException('The page no exists');
            }
        } else {
            return $this->redirect($this->generateUrl('sonata_user_admin_security_login'));
        }

        return $this->render('PDOneBundle:BrandPage:'.$template.'.html.twig', array(
            'email' => $entityMail,
            'company' => $entityCompany,
            'brandObj' => $entityBrand,
            'rep' => $entityRep,
            'target' => $entityTarget,
            'brand' => $brand_id,
            'medias' => $medias,
            'modal' => $modal,
        ));
    }

    /**
     * @Route("/unsub/{email_id}/{target_id}/{template}", name="unsub")
     * @Method("GET")
     * @Template()
     */
    public function unsubAction(Request $request, $email_id, $target_id, $template)
    {
        $em = $this->getDoctrine()->getManager();
        $target_id = $this->get('nzo_url_encryptor')->decrypt($target_id);
        $entityTarget = $em->getRepository('PDOneBundle:Target')->find($target_id);

        return $this->render('PDOneBundle:BrandPage:'.$template.'_unsub.html.twig', array(
            'target' => $entityTarget,
            'email_id' => $email_id,
        ));
    }

    /**
     * @Route("/save_unsub", name="save-unsub")
     * @Method("POST")
     */
    public function saveUnsubAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $status = null;
        $response['success'] = true;
        $target_id = $this->get('nzo_url_encryptor')->decrypt($request->get('target_id'));
        $target_mail = $this->get('nzo_url_encryptor')->decrypt($request->get('target_mail'));
        $option = $request->get('option');

        try {
            $entityAction = new Entity\Action();
            $entityAction->setTargetId($target_id);
            $entityAction->setActionType($option);
            $entityAction->setActionInfo($target_mail);
            $em->persist($entityAction);
            $em->flush();
        } catch (Exception $e) {
            $status = 400;
            $response['error'] = $e->getMessage();
        }

        return new JsonResponse($response, $status ?: 200);
    }

    /**
     * @Route("/show/{media_id}/{rep_id}/{email_id}", name="show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($media_id, $rep_id, $email_id)
    {
        $em = $this->getDoctrine()->getManager();
        $media_id = $this->get('nzo_url_encryptor')->decrypt($media_id);
        $rep_id = $this->get('nzo_url_encryptor')->decrypt($rep_id);
        $email_id = $this->get('nzo_url_encryptor')->decrypt($email_id);
        $entityMedia = $em->getRepository('PDOneBundle:Media')->find($media_id);
        $entityRep = $em->getRepository('PDOneBundle:Representative')->find($rep_id);
        $entityMail = $em->getRepository('PDOneBundle:Email')->find($email_id);
        $entityTarget = $entityMail->getTarget();
        $mediasXref = $entityMail->getEmailsMediaXref();
        $brand_id = 0;
        $template = 'show';
        $rep_id = $entityRep->getVeevaRepId();
        $target_id = $entityTarget->getVeevaAccountId();

        if ($mediasXref[0]) {
            $brand_id = $mediasXref[0]->getBrand()->getId();
        }
        $entityBrand = $em->getRepository('PDOneBundle:Brand')->find($brand_id);
        if ($entityBrand) {
            $template = $entityBrand->getTemplateName();
        }
        $track = $em->getRepository('PDOneBundle:Event')->findOneBy(
            array(
                'event_type' => 'media_view',
                'rep_id' => $rep_id,
                'target_id' => $target_id,
                'brand_id' => $brand_id,
                'email_id' => $email_id,
                'media_id' => $media_id,
            ),
            array('createdAt' => 'DESC')
        );

        $now = new \DateTime();

        if ($track === null) {
            $event = $this->saveEvent('media_view', $rep_id, $target_id, $brand_id, $email_id, $media_id, $now);
            $em->persist($event);
            $em->flush();
        }

        return $this->render('PDOneBundle:BrandPage:'.$template.'_show.html.twig', array(
            'media' => $entityMedia,
            'rep' => $entityRep,
            'email' => $entityMail,
            'target' => $entityTarget,
            'brand' => $brand_id,
        ));
    }

    /**
     * @Route("/question", name="question")
     * @Method("POST")
     */
    public function questionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $status = null;
        $response['success'] = true;
        $now = new \DateTime();
        $rep_id = $this->get('nzo_url_encryptor')->decrypt($request->get('rep_id'));
        $target_id = $this->get('nzo_url_encryptor')->decrypt($request->get('target_id'));
        $brand_id = $this->get('nzo_url_encryptor')->decrypt($request->get('brand_id'));
        $email_id = $this->get('nzo_url_encryptor')->decrypt($request->get('email_id'));
        $messages = $request->get('messages');
        $phone = '';

        if ($request->get('phone')) {
            $phone = '{"phone_number":"'.$request->get('phone').'"}';
        }

        $entityRep = $em->getRepository('PDOneBundle:Representative')->find($rep_id);
        $entityTarget = $em->getRepository('PDOneBundle:Target')->find($target_id);
        $event = $this->saveEvent('rep_contact', $rep_id, $target_id, $brand_id, $email_id, null, $now);
        $em->persist($event);
        $em->flush();

        try {
            $entityEmail = new Entity\Email();
            $entityEmail->setRep($entityRep);
            $entityEmail->setTarget($entityTarget);
            $entityEmail->setEmailsCategory('inbox');
            $entityEmail->setCreatedAt($now);
            $entityEmail->setDeliveredAt($now);
            $entityEmail->setViewedAt($now);
            $entityEmail->setData($phone);
            $entityMessage = null;

            if ($messages) {
                $entityMessage = $em->getRepository('PDOneBundle:Message')->find($messages);
                if ($entityMessage) {
                    $entityEmail->addEmailsMessageXref($entityMessage);
                }
            }

            $em->persist($entityEmail);
            $em->flush();
        } catch (Exception $e) {
            $status = 400;
            $response['error'] = $e->getMessage();
        }

        return new JsonResponse($response, $status ?: 200);
    }

    /**
     * @Route("/question-quillivant", name="question-quillivant")
     * @Method("POST")
     */
    public function questionQuillivantAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $status = null;
        $response['success'] = true;
        $now = new \DateTime();
        $rep_id = $this->get('nzo_url_encryptor')->decrypt($request->get('rep_id'));
        $target_id = $this->get('nzo_url_encryptor')->decrypt($request->get('target_id'));
        $brand_id = $this->get('nzo_url_encryptor')->decrypt($request->get('brand_id'));
        $email_id = $this->get('nzo_url_encryptor')->decrypt($request->get('email_id'));
        $messages = $request->get('messages');

        $entityRep = $em->getRepository('PDOneBundle:Representative')->find($rep_id);
        $entityTarget = $em->getRepository('PDOneBundle:Target')->find($target_id);
        $event = $this->saveEvent('rep_contact', $rep_id, $target_id, $brand_id, $email_id, null, $now);
        $em->persist($event);
        $em->flush();

        try {
            $entityEmail = new Entity\Email();
            $entityEmail->setRep($entityRep);
            $entityEmail->setTarget($entityTarget);
            $entityEmail->setEmailsCategory('inbox');
            $entityEmail->setCreatedAt($now);
            $entityEmail->setDeliveredAt($now);
            $entityEmail->setViewedAt($now);
            $entityMessage = null;

            if ($messages) {
                $entityMessage = $em->getRepository('PDOneBundle:Message')->find($messages);
                if ($entityMessage) {
                    $entityEmail->addEmailsMessageXref($entityMessage);
                }
            }

            $em->persist($entityEmail);
            $em->flush();
        } catch (Exception $e) {
            $status = 400;
            $response['error'] = $e->getMessage();
        }

        return new JsonResponse($response, $status ?: 200);
    }

    /**
     * @Route("/unsupport/{template}", name="unsupport-browser")
     * @Method("GET")
     * @Template()
     */
    public function unsupportBrowserAction(Request $request, $template)
    {
        return $this->render('PDOneBundle:BrandPage:'.$template.'_unsupport_browser.html.twig');
    }

    private function saveEvent($event_type, $rep_id, $target_id, $brand_id, $email_id, $media_id, $now)
    {
        $event = new Entity\Event();
        $event->setEventType($event_type);
        $event->setRepId($rep_id);
        $event->setTargetId($target_id);
        $event->setBrandId($brand_id);
        $event->setEmailId($email_id);
        $event->setMediaId($media_id);
        $event->setCreatedAt($now);

        return $event;
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     *
     * @return array
     */
    private function getFormErrors(\Symfony\Component\Form\Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrors($child);
            }
        }

        return $errors;
    }
}

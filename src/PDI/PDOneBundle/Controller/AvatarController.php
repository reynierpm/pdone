<?php

/**
 * Controller:    CompanyAdmin.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class AvatarController extends Controller
{
    public function changeAvatarAction()
    {
        $response['success'] = true;
        $status = null;
        $modelManager = $this->admin->getModelManager();
        $object = $this->admin->getSubject();
        try {
            if (!$object) {
                $status = 400;
                $response['success'] = false;
            } else {
                $object->setAvatarUrl('https://pdone.s3.amazonaws.com/avatar/default_avatar.png');
                // $object->setAvatarUrl('https://pdone.s3.amazonaws.com/avatar/no_avatar.png');
                $modelManager->update($object);
            }
        } catch (\Exception $e) {
            $status = 400;
            $response['error'] = $ex->getMessage();
        }

        return new JsonResponse($response, $status ?: 200);
    }
}

<?php

/**
 * Controller:    EmailAdmin.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Controller\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class EmailAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Information', array('class' => 'col-md-6'))
                ->add('emails_category')
                ->add('rep', 'sonata_type_model_list', array(
                        'label' => 'Representative',
                        'btn_add' => 'Add',      //Specify a custom label
                        'btn_list' => 'Select',     //which will be translated
                        'btn_delete' => false,             //or hide the button.
                    ), array(
                        'placeholder' => 'No Representative selected',
                    ))
                ->add('target', 'sonata_type_model_list', array(
                        'btn_add' => 'Add',      //Specify a custom label
                        'btn_list' => 'Select',     //which will be translated
                        'btn_delete' => false,             //or hide the button.
                    ), array(
                        'placeholder' => 'No Target selected', 'label' => 'Target',
                    ))
            ->end()
            // ->with('Time', array('class' => 'col-md-6'))
            //     ->add('deliveredAt')
            //     ->add('viewedAt')
            // ->end()
            ->with('Medias', array('class' => 'col-md-6'))
                ->add('emailsMediaXref', null, array('required' => false, 'label' => 'Select Medias'))
            ->end()
            ->with('Messages', array('class' => 'col-md-6'))
                ->add('emailsMessageXref', null, array('required' => false, 'label' => 'Select Messages'))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('emails_category')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('emails_category')
            ->add('rep')
            ->add('target')
            ->add('deliveredAt')
            ->add('viewedAt')
             // add custom action links
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('emails_category')
            ->add('deliveredAt')
            ->add('viewedAt')
            ->add('rep')
            ->add('target')
            ->add('emailsMediaXref')
            ->add('emailsMessageXref')
        ;
    }
}

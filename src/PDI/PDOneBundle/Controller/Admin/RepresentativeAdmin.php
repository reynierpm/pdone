<?php

/**
 * Controller:    RepresentativeAdmin.
 *
 * @author        Reynier Perez <reynier@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Controller\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class RepresentativeAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $image = $this->getSubject();
        $fileFieldOptions = array('required' => true, 'label' => 'Avatar');
        if ($image && ($webPath = $image->getAvatarUrl())) {
            // $fullPath = 'http://pdone.s3-website-us-east-1.amazonaws.com/'.$webPath;
            $fileFieldOptions['help'] = '<img src="'.$webPath.'" class="admin-preview img-rounded img-responsive"/>';
        }

        $formMapper
            ->with('Personal Information', array('class' => 'col-md-6'))
                ->add('display_name')
            ->end()
            ->with('Avatar', array('class' => 'col-md-6'))
                 ->add('file', 'file', $fileFieldOptions)
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('display_name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('avatar_url', null, array('label' => 'Photo'))
            ->addIdentifier('display_name')
            ->add('username')
            ->add('first')
            ->add('last')
            ->add('phone')
            ->add('email')
            ->add('inactive')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('display_name')
            ->add('avatar_url')
            ->add('rep_type')
            ->add('username')
            ->add('first')
            ->add('last')
            ->add('title')
            ->add('bio')
            ->add('phone')
            ->add('email')
            ->add('inactive')
            ->add('lastLoginAt')
            ->add('lastVeevaSyncAt')
            ->add('territory')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // Only `list` and `edit` route will be active
        $collection->clearExcept(array('list', 'edit', 'view'));
        $collection->add('changeAvatar', $this->getRouterIdParameter().'/changeAvatar');
    }

    public function prePersist($representative)
    {
        $this->manageFileUpload($representative);
    }

    public function preUpdate($representative)
    {
        $this->manageFileUpload($representative);
    }

    private function manageFileUpload($representative)
    {
        if ($representative->getFile()) {
            $representative->upload();
        }
    }
}

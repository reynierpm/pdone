<?php

/**
 * Controller:    TerritoryBrandAdmin.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Controller\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class TerritoryBrandAdmin extends Admin
{
    protected $parentAssociationMapping = 'Brand';
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Information', array('class' => 'col-md-6'))
                ->add('priority')
                // ->add('territory', 'sonata_type_model_list', array(
                //         'btn_add' => 'Add',      //Specify a custom label
                //         'btn_list' => 'Select',     //which will be translated
                //         'btn_delete' => false,             //or hide the button.
                //     ), array(
                //         'placeholder' => 'No Target selected',
                //     ))
                ->add('brand', 'sonata_type_model_list', array(
                        'btn_add' => 'Add',      //Specify a custom label
                        'btn_list' => 'Select',     //which will be translated
                        'btn_delete' => false,             //or hide the button.
                    ), array(
                        'placeholder' => 'No Brand selected',
                    ))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('priority')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('territory')
            ->add('brand')
            ->add('priority')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('territory')
            ->add('brand')
            ->add('priority')
        ;
    }
}

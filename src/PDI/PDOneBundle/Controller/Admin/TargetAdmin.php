<?php

/**
 * Controller:    TargetAdmin.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Controller\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TargetAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $image = $this->getSubject();
        $fileFieldOptions = array('required' => false, 'label' => 'Avatar');
        if ($image && ($webPath = $image->getAvatarUrl())) {
            $fileFieldOptions['help'] = '<img src="'.$webPath.'" class="admin-preview img-rounded img-responsive"/>';
        }

        $formMapper
            ->with('Information', array('class' => 'col-md-6'))
                ->add('display_name')
                ->add('first')
                ->add('last')
                ->add('phone')
                ->add('email')
                ->add('fax')
                ->add('file', 'file', $fileFieldOptions)
            ->end()
            ->with('Address', array('class' => 'col-md-6'))
                ->add('city')
                ->add('state')
                ->add('address1')
                ->add('address2')
                ->add('zip')
                ->add('state')
                ->add('territory', 'sonata_type_model_list', array(
                    'btn_add' => 'Add',      //Specify a custom label
                    'btn_list' => 'Select',     //which will be translated
                    'btn_delete' => false,             //or hide the button.
                    ), array(
                    'placeholder' => 'No Brand selected',
                ))
            ->end()
            // ->with('Other ', array('class' => 'col-md-6'))
                // ->add('veeva_account_id')
                // ->add('veeva_timestamp')
                // ->add('practioner_id')
                // ->add('inactive')
                // ->add('lastSyncAt')
            // ->end()
            ->with('Education', array('class' => 'col-md-6'))
                ->add('title')
                ->add('state_licensed')
                ->add('state_licensed_id')
                ->add('target_type')
                ->add('npi')
                ->add('suffix')
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('display_name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('last')
            ->add('first')
            ->add('city')
            ->add('state')
            ->add('zip')
            ->add('inactive')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            // ->add('veeva_account_id')
            // ->add('veeva_timestamp')
            ->add('display_name')
            ->add('avatar_url')
            ->add('title')
            ->add('first')
            ->add('last')
            ->add('suffix')
            ->add('address1')
            ->add('address2')
            ->add('city')
            ->add('zip')
            ->add('phone')
            ->add('fax')
            ->add('email')
            ->add('state_licensed')
            ->add('state_licensed_id')
            ->add('target_type')
            ->add('npi')
            ->add('state')
            ->add('practioner_id')
            ->add('inactive')
            ->add('lastSyncAt')
            ->add('territory')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // Only `list` and `edit` route will be active
        $collection->clearExcept(array('list', 'edit', 'show'));
    }

    public function prePersist($target)
    {
        $this->manageFileUpload($target);
    }

    public function preUpdate($target)
    {
        $this->manageFileUpload($target);
    }

    private function manageFileUpload($target)
    {
        if ($target->getFile()) {
            $target->upload();
        }
    }
}

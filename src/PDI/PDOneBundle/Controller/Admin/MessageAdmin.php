<?php

/**
 * Controller:    MessageAdmin.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Controller\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class MessageAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Message', array('class' => 'col-md-6'))
                ->add('category')
                ->add('email_template')
                ->add('message_subject')
                ->add('message_code')
                ->add('message_text')
            ->end()
            ->with('Inactive', array('class' => 'col-md-3'))
                ->add('inactive', null, array('required' => false))
            ->end()
            ->with('Content Share', array('class' => 'col-md-3'))
                ->add('content_share_required', null, array('required' => false))
            ->end()
            ->with('Brand', array('class' => 'col-md-6'))
                ->add('brand', 'sonata_type_model_list', array(
                        'btn_add' => 'Add',      //Specify a custom label
                        'btn_list' => 'Select',     //which will be translated
                        'btn_delete' => false,             //or hide the button.
                        ), array(
                        'placeholder' => 'No Brand selected',
                ))
            ->end();
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('message_text')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('message_code')
            ->add('content_share_required')
            ->add('category')
            ->add('brand')
            ->add('inactive')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('message_text')
            ->add('message_code')
            ->add('content_share_required')
            ->add('category')
            ->add('inactive')
            ->add('brand')
            ->add('messageXrefMail')
        ;
    }
}

<?php

/**
 * Controller:    BrandAdmin.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Controller\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class BrandAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $image = $this->getSubject();
        $logoOptions = array('required' => false, 'label' => 'Logo');
        $isiOptions = array('required' => false, 'label' => 'ISI PDF');
        $piOptions = array('required' => false, 'label' => 'PI PDF');
        if ($image && ($webPath = $image->getLogoUrl())) {
            $logoOptions['help'] = '<img src="'.$webPath.'" class="admin-preview img-rounded img-responsive"/>';
            $isiOptions['help'] = 'Name: '.$image->getIsiPdfUrl();
            $piOptions['help'] = 'Name: '.$image->getPiPdfUrl();
        }
        // see available options below
        $formMapper
            ->with('Information', array('class' => 'col-md-6'))
                ->add('name')
                ->add('generic_name', null, array(
                    'required' => false
                ))
                ->add('description', null, array(
                    'required' => false
                ))
                ->add('inactive', null, array('required' => false))
            ->end()
            ->with('Logo', array('class' => 'col-md-6'))
                ->add('file', 'file', $logoOptions)
                 ->add('company', 'sonata_type_model_list', array(
                        'btn_add' => 'Add',      //Specify a custom label
                        'btn_list' => 'Select',     //which will be translated
                        'btn_delete' => 'Delete',   //or hide the button.
                        'required' => false
                    ), array(
                        'placeholder' => 'No Company selected',
                    ))
            ->end()
            ->with('Isi Information', array('class' => 'col-md-6'))
                ->add('isi_required', null, array('required' => false))
                ->add('isi_text')
                ->add('isi_file', 'file', $isiOptions)
            ->end()
            ->with('Pi Information', array('class' => 'col-md-6'))
                ->add('pi_required', null, array('required' => false))
                ->add('pi_text')
                ->add('pi_file', 'file', $piOptions)
            ->end();
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('generic_name')
            ->add('company')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('generic_name')
            ->add('logo_url')
             // add custom action links
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('generic_name')
            ->add('logo_url')
            ->add('description')
            ->add('isi_required')
            ->add('isi_text')
            ->add('isi_pdf_url')
            ->add('pi_required')
            ->add('pi_text')
            ->add('pi_pdf_url')
            ->add('company')
            ->add('targetBrand')
            ->add('territoryBrand')
        ;
    }

    public function prePersist($brand)
    {
        $this->manageFileUpload($brand);
    }

    public function preUpdate($brand)
    {
        $this->manageFileUpload($brand);
    }

    private function manageFileUpload($brand)
    {
        if ($brand->getFile()) {
            $brand->upload();
        }
    }
}

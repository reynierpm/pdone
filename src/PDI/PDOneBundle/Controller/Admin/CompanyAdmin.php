<?php

/**
 * Controller:    CompanyAdmin.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Controller\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CompanyAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        // get the current Image instance
        $image = $this->getSubject();
        $fileFieldOptions = array('required' => false, 'label' => 'Logo');
        if ($image && ($webPath = $image->getLogoUrl())) {
            $fileFieldOptions['help'] = '<img src="'.$webPath.'" class="admin-preview img-rounded img-responsive"/>';
        }

        $em = $this->modelManager->getEntityManager('PDI\PDOneBundle\Entity\Brand');
        $obj = $this->getSubject();

        if(empty($obj->getId())) {
            $query = $em->createQueryBuilder('b')
                ->select('b')
                ->from('PDOneBundle:Brand', 'b')
                ->where('b.company IS NULL');
        } else {
            $query = $em->createQueryBuilder('b')
                ->select('b')
                ->from('PDOneBundle:Brand', 'b')
                ->where('b.company = :company_id');

            $query->setParameter('company_id', $obj->getId());
        }

        $formMapper
            ->with('Information', array('class' => 'col-md-6'))
            ->add('name')
            ->add('division')
            ->add('inactive', null, array(
                'required' => false
            ))
            ->add(
                'brands',
                'sonata_type_model',
                array(
                    'multiple' => true,
                    'by_reference' => false,
                    'query' => $query,
                    'required' => false
                )
            )
            ->end()
            ->with('Logo', array('class' => 'col-md-6'))
            ->add('file', 'file', $fileFieldOptions)
            ->end();
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('logo_url')
            ->add('division')
            ->add('inactive')
            // add custom action links
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                    ),
                )
            );
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('logo_url')
            ->add('division')
            ->add('inactive');
    }

    public function prePersist($company)
    {
        $this->manageFileUpload($company);
        $this->preUpdate($company);
    }

    public function preUpdate($company)
    {
        $this->manageFileUpload($company);
        $company->setBrands($company->getBrands());
    }

    private function manageFileUpload($company)
    {
        if ($company->getFile()) {
            $company->upload();
        }
    }
}

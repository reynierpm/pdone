<?php

/**
 * Controller:    MediaAdmin.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Controller\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class MediaAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $image = $this->getSubject();
        $fileFieldOptions = array('required' => false, 'label' => 'Thumbnail');
        $fileMediaFieldOptions = array('required' => true, 'label' => 'Media File');

        if ($image && ($webPath = $image->getThumbnailUrl())) {
            $fileFieldOptions['help'] = 'Thumbnail: <img src="'.$webPath.'" class="admin-preview img-rounded img-responsive"/>';
            $fileMediaFieldOptions['help'] = 'Type: '.$image->getMediaType().'<br/>Name: '.$image->getMediaUrl();
        }

        $formMapper
            ->with('Information', array('class' => 'col-md-6'))
                ->add('title', null, array(
                    'label' => 'Title (text format)',
                    'required' => true,
                ))
                ->add('titleHtml', 'ckeditor', array(
                    'config_name' => 'default_config',
                    'label' => 'Title (HTML format)',
                ))
                ->add('description', null, array(
                    'label' => 'Description (text format)',
                    'required' => true,
                ))
                ->add('descriptionHtml', 'ckeditor', array(
                    'config_name' => 'default_config',
                    'label' => 'Description (HTML format)',
                    'attr' => array(
                        'row' => 5,

                ), ))
                ->add('useHtml', null, array('required' => false))
                ->add('inactive', null, array('required' => false))
            ->end()
            ->with('Brand', array('class' => 'col-md-6'))
                ->add('brand', 'sonata_type_model_list', array(
                        'btn_add' => 'Add',      //Specify a custom label
                        'btn_list' => 'Select',     //which will be translated
                        'btn_delete' => false,             //or hide the button.
                        ), array(
                        'placeholder' => 'No Brand selected',
                        'required' => true,
                ))
            ->end()
            ->with('Media', array('class' => 'col-md-6'))
                ->add('file', 'file', $fileFieldOptions)
                ->add('media_file', 'file', $fileMediaFieldOptions)
                ->add('media_type', 'choice', array(
                    'placeholder' => 'Choose an media type',
                    'choices' => array(
                        'PDF'   => 'PDF',
                        'VIDEO' => 'VIDEO',
                        'IMAGE'   => 'IMAGE',
                    ),
                    'required' => true,
                ))
                ->add('media_code', null, array(
                    'required' => true
                ))
            ->end();
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('media_type');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('media_type')
            ->add('title')
            ->add('media_code')
            ->add('brand')
            ->add('inactive')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('media_type')
            ->add('title')
            ->add('description')
            ->add('thumbnail_url')
            ->add('media_url')
            ->add('media_code')
            ->add('inactive')
            ->add('brand')
            ->add('mediaXrefMail');
    }

    public function prePersist($media)
    {
        $this->manageFileUpload($media);
    }

    public function preUpdate($media)
    {
        $this->manageFileUpload($media);
    }

    private function manageFileUpload($media)
    {
        if ($media->getBrand() != null) {
            $media->upload();
        }
    }
}

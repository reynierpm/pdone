$('#video_ie').hide();

$('#aboutOptions').on('change', function() {
    if($(this).val() !=0){
        $('#sendAskRequestQuillivant').prop( "disabled", false );
    }else{
        $('#sendAskRequestQuillivant').prop( "disabled", true );
    }
});

$('#visit').on('click', function (e) {
    $('#sendAskRequestQuillivant').prop( "disabled", false );
});

$('#questions').on('click', function (e) {
    $('#sendAskRequestQuillivant').prop( "disabled", false );
});

$('#askCollapse').on('hidden.bs.collapse', function () {
    $('#content').show();
    $('#content-message').hide();
});

$('#askCollapse').on('show.bs.collapse', function () {
    $('#content').show();
    $('#content-message').hide();
});

$('#aboutOptionsModal').on('change', function() {
    if($(this).val() !=0){
        $('#sendAskRequestModalQuillivant').prop( "disabled", false );
    }else{
        $('#sendAskRequestModalQuillivant').prop( "disabled", true );
    }
});

$('#visitModal').on('click', function (e) {
    $('#sendAskRequestModalQuillivant').prop( "disabled", false );
});

$('#questionsModal').on('click', function (e) {
    $('#sendAskRequestModalQuillivant').prop( "disabled", false );
});

$('#askModal').on('hidden.bs.modal', function (e) {
    if (detectIE()) {
        $('.modal').removeClass('fade');
        $('#pdf').show();
    }
    resetModelContent();
    $('#contentModal').show();
    $('#content-messageModal').hide();

});

$('#askModal').on('shown.bs.modal', function (e) {
    if (detectIE()) {
        $('.modal').removeClass('fade');
        $('#pdf').hide();
    }
    $('#contentModal').show();
    $('#content-messageModal').hide();
});

$('#askModal').on('show.bs.modal', function (e) {
    if (detectIE()) {
        $('.modal').removeClass('fade');
        $('#pdf').hide();
    }
    $('#contentModal').show();
    $('#content-messageModal').hide();
});
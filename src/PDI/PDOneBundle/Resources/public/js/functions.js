    function postData( url, data, type, name, e ){
        $.post(url, data, 'json').done(function (data, textStatus, jqXHR) {
            reset();
            $("#h2Modal").html("Your request has been sent.");
            $("#h2Collapse").html("Your request has been sent.");
        }).fail(function () {
            reset();
            $("#h2Modal").html("Your request not has been sent.");
            $("#h2Collapse").html("Your request not has been sent.");
            e.preventDefault();
        });
    }

    function reset () {
        $('input[type=radio]').each(function () {
                $(this).removeAttr('checked');
        });
        $("#aboutOptions").val(0);
        $("#aboutOptionsModal").val(0);
        $("#phone").prop( "disabled", true );
        $("#phone").val('');
        $("#phoneModal").prop( "disabled", true );
        $("#phoneModal").val('');
        $('#sendAskRequest').prop( "disabled", true );
        $('#sendAskRequestModal').prop( "disabled", true );
        resetPhone();
        resetModelContent();
    }

    function resetModelContent(){
        $('#content-messageModal').show('slow');
        $('#contentModal').hide('slow');
        $('#content-message').show('slow');
        $('#content').hide('slow');
    }

    function resetPhone () {
        $("#visitLabel").removeClass("state-error");
        $("#visitLabel").removeClass("state-success");
        $("#error").hide();
        $("#visitLabelModal").removeClass("state-error");
        $("#visitLabelModal").removeClass("state-success");
        $("#errorModal").hide();
    }

    function checkBrowser(isIE, url){
        if(typeof isIE != "undefined"){
            document.write("redirecting...");
            var referLink = document.createElement("a");
            referLink.href = url;//"{{ path('unsupport-browser', {'template': 'quillivant' }) }}";
            document.body.appendChild(referLink);
            referLink.click();
        }
    }

    function detectIE() {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
           // IE 12 => return version number
           return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        // other browser
        return false;
    }
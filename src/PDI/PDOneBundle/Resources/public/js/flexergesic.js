    $('#visit').on('click', function (e) {
        $("#phone").prop( "disabled", true );
        $("#phone").val('');
        resetPhone () ;
        $('#sendAskRequest').prop( "disabled", false );
    });

    $('#questions').on('click', function (e) {
        $("#phone").prop( "disabled", false );
        $('#sendAskRequest').prop( "disabled", false );
    });

    $('#askCollapse').on('hidden.bs.collapse', function () {
        $('#content').show();
        $('#content-message').hide();
    });

    $('#askCollapse').on('show.bs.collapse', function () {
        $('#content').show();
        $('#content-message').hide();
    });

    $('#visitModal').on('click', function (e) {
        $("#phoneModal").prop( "disabled", true );
        $("#phoneModal").val('');
        resetPhone() ;
        $('#sendAskRequestModal').prop( "disabled", false );
    });

    $('#questionsModal').on('click', function (e) {
        $("#phoneModal").prop( "disabled", false );
        $('#sendAskRequestModal').prop( "disabled", false );
    });

    $('#askModal').on('hidden.bs.modal', function (e) {
        if (detectIE()) {
            $('.modal').removeClass('fade');
            $('#pdf').show();
        }
        reset();
        $('#contentModal').show();
        $('#content-messageModal').hide();
    });

    $('#askModal').on('shown.bs.modal', function (e) {
        $('#contentModal').show();
        $('#content-messageModal').hide();
        if (detectIE()) {
            $('.modal').removeClass('fade');
            $('#pdf').show();
        }
    });

    $('#askModal').on('show.bs.modal', function (e) {
        if (detectIE()) {
            $('.modal').removeClass('fade');
            $('#pdf').show();
        }
        $('#contentModal').show();
        $('#content-messageModal').hide();
    });

    jQuery(function($){
        $("#phone").mask("(999) 999-9999",{placeholder:"_"});
        $("#phoneModal").mask("(999) 999-9999",{placeholder:"_"});
    });
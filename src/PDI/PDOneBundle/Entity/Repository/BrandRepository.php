<?php

/**
 * Repository:    Brand.
 *
 * @author        Reynier Perez <reynier@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class BrandRepository extends EntityRepository
{
    /**
     * Gets brand by target.
     *
     * @param $tid
     *
     * @return mixed
     */
    public function getBrandsByTarget($tid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT brands.* FROM brands LEFT JOIN targets_brands_xref ON (targets_brands_xref.brands_id = brands.id) LEFT JOIN targets ON (targets.id = targets_brands_xref.targets_id) WHERE targets.id = :tid');

        $stmt->bindValue('tid', $tid);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}

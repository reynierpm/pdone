<?php

/**
 * Repository:    Representative.
 *
 * @author        Reynier Perez <reynier@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class RepresentativeRepository extends EntityRepository
{
    /**
     * Gets all media by reps.
     *
     * @param $trid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllMedia($trid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT DISTINCT media.id, media.title, media.description, media.thumbnail_url FROM media LEFT JOIN territories_brands_xref on territories_brands_xref.brands_id = media.brands_id LEFT JOIN reps ON reps.territories_id = territories_brands_xref.territories_id WHERE reps.territories_id = :trid');

        $stmt->bindValue('trid', $trid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets the total number of targets in the reps territory.
     *
     * @param $rid
     *
     * @return One column of the next row specified by column index
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getRepsAction() and putRepsAction()
     */
    public function getTotalContacts($rid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT COUNT(targets.target_id) FROM targets LEFT JOIN territories ON (targets.territories_id = territories.territory_id) LEFT JOIN reps ON (reps.territories_id = territories.territory_id) WHERE reps.rep_id = :rid');

        $stmt->bindValue('rid', $rid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total number of emails that rep has sent out.
     *
     * @param $rid
     *
     * @return One column of the next row specified by column index
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getRepsAction() and putRepsAction()
     */
    public function getTotalShares($rid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT count(*) from emails_media_xref LEFT JOIN emails on emails.id = emails_media_xref.emails_id where emails.reps_id = :rid');

        $stmt->bindValue('rid', $rid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets media last share for reps.
     *
     * @param $rid
     * @param $mid
     * @param $etype
     *
     * @return One column of the next row specified by column index
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getRepsActivityAction()
     */
    public function getLastMediaCreatedAtByEventType($rid, $mid, $etype)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT events_tracking.createdAt FROM events_tracking WHERE events_tracking.rep_id = :rid AND events_tracking.event_type = :etype AND events_tracking.media_id = :mid ORDER BY events_tracking.createdAt DESC LIMIT 1');

        $stmt->bindValue('rid', $rid);
        $stmt->bindValue('mid', $mid);
        $stmt->bindValue('etype', $etype);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total views.
     *
     * @param $rid
     *
     * @return One column of the next row specified by column index
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getRepsAction() and putRepsAction()
     */
    public function getTotalViews($rid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT COUNT(*) FROM events_tracking WHERE events_tracking.rep_id = :rid AND events_tracking.event_type = "media_view" AND events_tracking.media_id in (SELECT media.id FROM media LEFT JOIN territories_brands_xref ON territories_brands_xref.brands_id = media.brands_id LEFT JOIN reps on reps.territories_id = territories_brands_xref.territories_id)');

        $stmt->bindValue('rid', $rid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total shares of the media by reps.
     *
     * @param int $rid
     * @param int $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getTotalMediaShares($rid, $mid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT COUNT(*) from emails_media_xref LEFT JOIN emails on emails.id = emails_media_xref.emails_id where emails.reps_id = :rid and emails_media_xref.media_id = :mid');

        $stmt->bindValue('rid', $rid);
        $stmt->bindValue('mid', $mid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total views of the media by reps.
     *
     * @param int $rid
     * @param int $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getTotalMediaViews($rid, $mid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT COUNT(*) FROM events_tracking WHERE events_tracking.rep_id = :rid AND events_tracking.event_type = "media_view" AND events_tracking.media_id = :mid');

        $stmt->bindValue('rid', $rid);
        $stmt->bindValue('mid', $mid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets reps activity.
     *
     * @param int $rid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRepsActivity($rid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            //->prepare('SELECT events_tracking.id, events_tracking.event_type, events_tracking.createdAt, targets.display_name, media.title, media.description, messages.message_subject, messages.message_text, emails.emails_category FROM events_tracking LEFT JOIN targets ON events_tracking.target_id = targets.id LEFT JOIN media ON events_tracking.media_id = media.id LEFT JOIN emails ON events_tracking.email_id = emails.id LEFT JOIN emails_messages_xref ON emails.id = emails_messages_xref.emails_id LEFT JOIN messages ON messages.id = emails_messages_xref.messages_id WHERE events_tracking.rep_id = :rid AND (events_tracking.event_type = "email_sent" OR events_tracking.event_type = "email_view" OR events_tracking.event_type = "page_view" OR events_tracking.event_type = "media_view" OR events_tracking.event_type = "rep_contact") ORDER BY events_tracking.createdAt DESC LIMIT 10');
            ->prepare('SELECT events_tracking.id, events_tracking.event_type, events_tracking.createdAt, targets.display_name, media.title, media.description, messages.id AS mid, messages.message_subject, messages.message_text, emails.data, emails.emails_category FROM events_tracking LEFT JOIN targets ON events_tracking.target_id = targets.target_id LEFT JOIN media ON events_tracking.media_id = media.id LEFT JOIN emails ON events_tracking.email_id = emails.id LEFT JOIN emails_messages_xref ON emails.id = emails_messages_xref.emails_id LEFT JOIN messages ON messages.id = emails_messages_xref.messages_id WHERE events_tracking.rep_id = :rid AND (events_tracking.event_type = "email_view" OR events_tracking.event_type = "media_view") ORDER BY events_tracking.createdAt DESC LIMIT 10');

        $stmt->bindValue('rid', $rid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets the latest 10 messags by reps.
     *
     * @param $rid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getMessagesByReps($rid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT messages.* FROM messages LEFT JOIN emails_messages_xref ON (messages.id = emails_messages_xref.messages_id) LEFT JOIN emails ON (emails_messages_xref.emails_id = emails.id) WHERE emails.reps_id = :rid LIMIT 10');

        $stmt->bindValue('rid', $rid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets representative territories.
     *
     * @param $rid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRepsTerritories($rid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT territories.* FROM territories LEFT JOIN reps ON (reps.territories_id = territories.territory_id) WHERE reps.rep_id = :rid');

        $stmt->bindValue('rid', $rid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets representative targets.
     *
     * @param $rid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRepsTargets($rid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT targets.* FROM targets LEFT JOIN territories ON (targets.territories_id = territories.territory_id) LEFT JOIN reps ON (reps.territories_id = territories.territory_id) WHERE reps.rep_id = :rid');

        $stmt->bindValue('rid', $rid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets all emails by reps.
     *
     * @param $rid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getRepsEmailsAction()
     */
    public function getAllEmail($rid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT emails.*, emails_messages_xref.messages_id AS messages_id, messages.brands_id, messages.message_subject, messages.message_text, messages.category, brands.name, targets.display_name FROM emails LEFT JOIN emails_messages_xref ON emails.id = emails_messages_xref.emails_id LEFT JOIN messages ON emails_messages_xref.messages_id = messages.id LEFT JOIN targets ON emails.targets_id = targets.target_id LEFT JOIN brands ON messages.brands_id = brands.id WHERE emails.reps_id = :rid');

        $stmt->bindValue('rid', $rid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets openedAt parameter for.
     *
     * @param $eid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getRepsEmailsAction()
     */
    public function getEmailOpenetAt($eid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT events_tracking.createdAt FROM events_tracking WHERE events_tracking.event_type = "page_view" AND events_tracking.email_id = :eid ORDER BY events_tracking.createdAt DESC LIMIT 1');

        $stmt->bindValue('eid', $eid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets viewedAt parameter for media.
     *
     * @param $eid
     * @param $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getRepsEmailsAction()
     */
    public function getMediaViewedAt($eid, $mid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT events_tracking.createdAt FROM events_tracking WHERE events_tracking.event_type = "media_view" AND events_tracking.email_id = :eid AND events_tracking.media_id = :mid ORDER BY events_tracking.createdAt DESC LIMIT 1');

        $stmt->bindValue('eid', $eid);
        $stmt->bindValue('mid', $mid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets viewedAt parameter for email.
     *
     * @param $eid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getRepsEmailsAction()
     */
    public function getEmailViewedAt($eid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT events_tracking.createdAt FROM events_tracking WHERE events_tracking.event_type = "email_view" AND events_tracking.email_id = :eid ORDER BY events_tracking.createdAt DESC LIMIT 1');

        $stmt->bindValue('eid', $eid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets all media for emails.
     *
     * @param $eid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getRepsEmailsAction()
     */
    public function getAllEmailsMedia($eid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT media.id, media.title, media.description, media.thumbnail_url FROM media LEFT JOIN emails_media_xref ON media.id = emails_media_xref.media_id LEFT JOIN emails ON emails_media_xref.emails_id = emails.id WHERE emails.id = :eid');

        $stmt->bindValue('eid', $eid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Find reps based on target.
     *
     * @param $tid
     *
     * @return One column of the next row specified by column index
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses postEmailsAction()
     */
    public function findRepByTarget($tid)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('r.rep_id', 'r.display_name', 'r.avatar_url')
            ->from('PDOneBundle:Representative', 'r')
            ->leftJoin('PDOneBundle:Territory', 't', \Doctrine\ORM\Query\Expr\Join::WITH, 't.territory_id = r.territory')
            ->leftJoin('PDOneBundle:Target', 'tg', \Doctrine\ORM\Query\Expr\Join::WITH, 'tg.territory = t.territory_id')
            ->where('tg.target_id = :tid')
            ->andWhere('r.inactive = 0')
            ->setParameter('tid', $tid);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Find target_id, email_id, rep_id values.
     *
     * @param $eid
     *
     * @return One column of the next row specified by column index
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getEmailAction()
     */
    public function findTargetEmailRepValues($eid)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('e.id', 'IDENTITY(e.rep) AS rep_id', 'IDENTITY(e.target) AS target_id')
            ->from('PDOneBundle:Email', 'e')
            ->leftJoin('PDOneBundle:Target', 'tg', \Doctrine\ORM\Query\Expr\Join::WITH, 'e.target = tg.target_id')
            ->where('e.id = :eid')
            ->setParameter('eid', $eid);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Find brand_id value.
     *
     * @param $eid
     *
     * @return One column of the next row specified by column index
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getEmailAction()
     */
    public function findBrandValues($eid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT messages.brands_id FROM emails JOIN emails_messages_xref ON emails_messages_xref.emails_id = emails.id JOIN messages ON messages.id = emails_messages_xref.messages_id WHERE emails.id = :eid');
            // ->prepare('SELECT messages.brands_id FROM emails JOIN emails_messages_xref ON emails_messages_xref.emails_id = emails.id JOIN messages ON messages.id = emails_messages_xref.messages_id = messages_id WHERE emails.id = :eid');

        $stmt->bindValue('eid', $eid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Find reps based on date range.
     *
     * @param $startD, $endD
     *
     * @return One column of the next row specified by column index
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function findRepByRange($startD, $endD)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT COUNT(reps.rep_id) FROM reps WHERE reps.createdAt BETWEEN :startD AND :endD');
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total number of emails sent out.
     *
     * @param $startD, $endD
     *
     * @return One column of the next row specified by column index
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function getTotalSharesByRange($startD, $endD)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT COUNT(*) from emails_media_xref LEFT JOIN emails on emails.id = emails_media_xref.emails_id WHERE emails.createdAt BETWEEN :startD AND :endD');
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total number of emails sent out.
     *
     * @param $startD, $endD, $endD, $bid, $tid, $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function getTotalEmailsSendByRange($startD, $endD, $bid = null, $tid = null, $mid = null)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';

        $bq = null;
        $mq = null;
        $tq = null;
        $query = 'SELECT COUNT(*) FROM events_tracking';
        if ($mid != null) {
            $query .= ' JOIN emails_media_xref on emails_media_xref.emails_id = events_tracking.email_id';
            $mq = ' AND emails_media_xref.media_id = '.$mid;
        }
        if ($bid != null) {
            $bq = ' AND events_tracking.brand_id ='.$bid;
        }
        if ($tid != null) {
            $query .= '  JOIN reps on reps.rep_id = events_tracking.rep_id';
            $tq = " AND reps.territories_id = '$tid'";
        }
        $query .= ' WHERE events_tracking.event_type = "email_sent" AND events_tracking.createdAt BETWEEN :startD AND :endD'.$mq.$bq.$tq;
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($query);
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total number of emails view.
     *
     * @param $startD, $endD, $endD, $bid, $tid, $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function getTotalEmailsViewByRange($startD, $endD, $bid = null, $tid = null, $mid = null)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';
        $custom = '';

        $bq = null;
        $mq = null;
        $tq = null;
        $query = 'SELECT COUNT(*) FROM events_tracking';
        if ($mid != null) {
            $query .= ' JOIN emails_media_xref on emails_media_xref.emails_id = events_tracking.email_id';
            $mq = ' AND emails_media_xref.media_id = '.$mid;
        }
        if ($bid != null) {
            $bq = ' AND events_tracking.brand_id ='.$bid;
        }
        if ($tid != null) {
            $query .= '  JOIN reps on reps.rep_id = events_tracking.rep_id';
            $tq = " AND reps.territories_id = '$tid'";
        }
        $query .= ' WHERE events_tracking.event_type = "email_view" AND events_tracking.createdAt BETWEEN :startD AND :endD'.$mq.$bq.$tq;
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($query);
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total number of pages view.
     *
     * @param $startD, $endD, $endD, $bid, $tid, $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function getTotalPagesViewByRange($startD, $endD, $bid = null, $tid = null, $mid = null)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';
        $bq = null;
        $mq = null;
        $tq = null;
        $query = 'SELECT COUNT(*) FROM events_tracking';
        if ($mid != null) {
            $query .= ' JOIN emails_media_xref on emails_media_xref.emails_id = events_tracking.email_id';
            $mq = ' AND emails_media_xref.media_id = '.$mid;
        }
        if ($bid != null) {
            $bq = ' AND events_tracking.brand_id ='.$bid;
        }
        if ($tid != null) {
            $query .= '  JOIN reps on reps.rep_id = events_tracking.rep_id';
            $tq = " AND reps.territories_id = '$tid'";
        }
        $query .= ' WHERE events_tracking.event_type = "page_view" AND events_tracking.createdAt BETWEEN :startD AND :endD'.$mq.$bq.$tq;
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($query);
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total number of media view.
     *
     * @param $startD, $endD, $bid, $tid, $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function getTotalMediaViewByRange($startD, $endD, $bid = null, $tid = null, $mid = null)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';
        $custom = '';

        if ($bid != null) {
            $custom .= ' AND events_tracking.brand_id ='.$bid;
        }
        if ($mid != null) {
            $custom .= ' AND events_tracking.media_id = '.$mid;
        }
        $query = 'SELECT COUNT(*) FROM events_tracking WHERE events_tracking.event_type = "media_view" AND events_tracking.createdAt BETWEEN :startD AND :endD'.$custom;

        if ($tid != null) {
            $custom .= " AND reps.territories_id = '$tid'";
            $query = 'SELECT COUNT(*) FROM events_tracking LEFT JOIN reps ON events_tracking.rep_id = reps.rep_id WHERE events_tracking.event_type = "media_view" AND events_tracking.createdAt BETWEEN :startD AND :endD'.$custom;
        }
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($query);
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total number of media view.
     *
     * @param $startD, $endD, $endD, $bid, $tid, $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function getTotalMediaSharesByRange($startD, $endD, $bid = null, $tid = null, $mid = null)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';
        $custom = '';

        if ($mid != null) {
            $custom .= ' AND emails_media_xref.media_id = '.$mid;
        }
        // $query = 'SELECT COUNT(*) from emails_media_xref LEFT JOIN emails on emails.id = emails_media_xref.emails_id WHERE emails.createdAt BETWEEN :startD AND :endD'.$custom;

        if ($bid != null) {
            $custom .= ' AND brands.id = '.$bid;
        }

        if ($tid != null) {
            $custom .= " AND territories.territory_id = '$tid'";
        }

        $query = 'SELECT COUNT(*) FROM emails_media_xref LEFT JOIN emails ON emails.id = emails_media_xref.emails_id LEFT JOIN reps ON emails.reps_id = reps.rep_id RIGHT JOIN territories ON reps.territories_id = territories.territory_id LEFT JOIN media ON emails_media_xref.media_id = media.id INNER JOIN brands ON media.brands_id = brands.id WHERE  emails.createdAt BETWEEN :startD AND :endD'.$custom;
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($query);
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total # of Targets Shared With.
     *
     * @param $startD, $endD, $endD, $bid, $tid, $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function getTotalTargetSharesByRange($startD, $endD, $bid = null, $tid = null, $mid = null)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';
        $custom = '';

        if ($bid != null) {
            $custom .= ' AND events_tracking.brand_id ='.$bid;
        }

        if ($mid != null) {
            $custom .= ' AND events_tracking.media_id = '.$mid;
        }

        $query = 'SELECT SUM(shares) FROM(SELECT COUNT(*) shares FROM events_tracking WHERE events_tracking.event_type = "email_sent" AND  events_tracking.createdAt BETWEEN :startD AND :endD '.$custom.' GROUP BY target_id ) as targetShare';

        if ($tid != null) {
            $custom .= " AND reps.territories_id = '$tid'";
            $query = 'SELECT SUM(shares) FROM(SELECT COUNT(*) shares FROM events_tracking LEFT JOIN reps ON events_tracking.rep_id = reps.rep_id WHERE events_tracking.event_type = "email_sent" AND  events_tracking.createdAt BETWEEN :startD AND :endD '.$custom.' GROUP BY target_id ) as targetShare';
        }
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($query);
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * The number of reps who have at least 1 media_share event in the last time period.
     *
     * @param $startD, $endD, $bid, $tid, $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function getRepsWhoShared($startD, $endD, $bid = null, $tid = null, $mid = null)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';
        $custom = '';

        if ($bid != null) {
            $custom .= ' AND events_tracking.brand_id ='.$bid;
        }

        if ($mid != null) {
            $custom .= ' AND events_tracking.media_id = '.$mid;
        }

        $query = 'SELECT COUNT(DISTINCT rep_id) FROM (SELECT rep_id FROM events_tracking WHERE events_tracking.event_type = "media_share" AND  events_tracking.createdAt BETWEEN :startD AND :endD '.$custom.') as week_of_media_share';

        if ($tid != null) {
            $custom .= " AND reps.territories_id = '$tid'";
            $query = 'SELECT COUNT(DISTINCT rep_id) FROM (SELECT events_tracking.rep_id FROM events_tracking LEFT JOIN reps ON events_tracking.rep_id = reps.rep_id WHERE events_tracking.event_type = "media_share" AND events_tracking.createdAt BETWEEN :startD AND :endD'.$custom.' ) as week_of_media_share';
        }

        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($query);
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     *  Total Reps.
     *
     * @param $startD, $endD, $bid, $tid, $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function getRepsInField($startD, $endD, $bid = null, $tid = null, $mid = null)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';
        $custom = '';

        if ($bid != null) {
            $custom .= ' AND events_tracking.brand_id ='.$bid;
        }

        if ($mid != null) {
            $custom .= ' AND events_tracking.media_id = '.$mid;
        }

        $query = 'SELECT COUNT(DISTINCT rep_id) FROM (SELECT events_tracking.rep_id FROM events_tracking WHERE events_tracking.createdAt BETWEEN :startD AND :endD '.$custom.') as week_of_rep_activity';

        if ($tid != null) {
            $custom .= " AND reps.territories_id = '$tid'";
            $query = 'SELECT COUNT(DISTINCT rep_id) FROM (SELECT events_tracking.rep_id FROM events_tracking LEFT JOIN reps ON events_tracking.rep_id = reps.rep_id WHERE events_tracking.createdAt BETWEEN :startD AND :endD'.$custom.' ) as week_of_rep_activity';
        }

        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($query);
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * The number of targets who have at least 1 media_view event in the last time period.
     *
     * @param $startD, $endD, $endD, $bid, $tid, $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses reportAction()
     */
    public function getUniqueTargetView($startD, $endD, $bid = null, $tid = null, $mid = null)
    {
        $startD .= ' 00:00:00';
        $endD .= ' 23:59:59';
        $custom = '';

        if ($bid != null) {
            $custom .= ' AND events_tracking.brand_id ='.$bid;
        }

        if ($mid != null) {
            $custom .= ' AND events_tracking.media_id = '.$mid;
        }
        $query = 'SELECT COUNT(DISTINCT target_id) FROM (SELECT target_id FROM events_tracking WHERE events_tracking.event_type = "media_view" AND  events_tracking.createdAt BETWEEN :startD AND :endD ) as week_of_media_views';

        if ($tid != null) {
            $custom .= " AND reps.territories_id = '$tid'";
            $query = 'SELECT COUNT(DISTINCT target_id) FROM (SELECT events_tracking.target_id FROM events_tracking LEFT JOIN reps ON events_tracking.rep_id = reps.rep_id WHERE events_tracking.event_type = "media_view" AND events_tracking.createdAt BETWEEN :startD AND :endD'.$custom.' ) as week_of_media_views';
        }
        $stmt = $this->getEntityManager()
            ->getConnection()
             ->prepare($query);
        $stmt->bindValue('startD', $startD);
        $stmt->bindValue('endD', $endD);
        $stmt->execute();

        return $stmt->fetchColumn();
    }
}

<?php

/**
 * Repository:    Representative.
 *
 * @author        Reynier Perez <reynier@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class TerritoryRepository extends EntityRepository
{
    /**
     * Gets territory targets.
     *
     * @param $tid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getTerritoryTargets($tid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT targets.* FROM targets LEFT JOIN territories ON (targets.territories_id = territories.territory_id) WHERE territories.territory_id = :tid');

        $stmt->bindValue('tid', $tid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets all companies.
     *
     * @param $tid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllCompanies($tid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT companies.id, companies.* FROM companies LEFT JOIN brands ON brands.companies_id = companies.id LEFT JOIN territories_brands_xref ON territories_brands_xref.brands_id = brands.id LEFT JOIN territories ON territories_brands_xref.territories_id = territories.territory_id WHERE territories.territory_id = :tid');

        $stmt->bindValue('tid', $tid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets all brands.
     *
     * @param $tid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllBrands($tid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT brands.id, brands.*, territories_brands_xref.priority FROM brands LEFT JOIN territories_brands_xref ON territories_brands_xref.brands_id = brands.id LEFT JOIN territories ON territories.territory_id = territories_brands_xref.territories_id WHERE territories.territory_id = :tid');

        $stmt->bindValue('tid', $tid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets all media.
     *
     * @param $tid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllMedia($tid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT media.id, media.* FROM media LEFT JOIN territories_brands_xref ON territories_brands_xref.brands_id = media.brands_id LEFT JOIN territories ON territories.territory_id = territories_brands_xref.territories_id WHERE territories.territory_id = :tid');

        $stmt->bindValue('tid', $tid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets all messages.
     *
     * @param $tid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllMessages($tid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT messages.id, messages.* FROM messages LEFT JOIN territories_brands_xref ON territories_brands_xref.brands_id = messages.brands_id LEFT JOIN territories ON territories.territory_id = territories_brands_xref.territories_id WHERE territories.territory_id = :tid');

        $stmt->bindValue('tid', $tid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets territory_id by target.
     *
     * @param $tgid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getTerritoryByTarget($tgid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT territories_id FROM targets WHERE target_id = :tgid');

        $stmt->bindValue('tgid', $tgid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets all brands by territory.
     *
     * @param $tid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getBrandsMediaByTerritory($tid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT DISTINCT brands.* FROM brands LEFT JOIN targets_brands_xref ON brands.id = targets_brands_xref.brands_id LEFT JOIN targets ON targets_brands_xref.targets_id = targets.id LEFT JOIN media ON brands.id = media.brands_id WHERE targets.territories_id = :tid LIMIT 10');

        $stmt->bindValue('tid', $tid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getTerritoryByFilter($filter)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('t')->from('PDOneBundle:Territory', 't');
        $qb->where('t.name LIKE ?1');
        $qb->setParameter(1, '%'.$filter.'%');

        return $qb->getQuery()->getResult();
    }

    /**
     * Gets territory targets.
     *
     * @param $tid, $email
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getTerritoryTargetsEmailStatus($tid, $email)
    {
        $qb = $this->createQueryBuilder('tq');
        $qb->select('ac.action_type')
            ->from('PDOneBundle:Action', 'ac')
            ->where('ac.target_id = :tid')
            ->andWhere($qb->expr()->eq('ac.action_info', ':email'))
            ->setMaxResults(1)
            ->setParameter('email', $email)
            ->setParameter('tid', $tid);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

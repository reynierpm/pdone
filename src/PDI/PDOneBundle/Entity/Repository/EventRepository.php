<?php

/**
 * Repository:        Event.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class EventRepository extends EntityRepository
{
    /**
     * Get all data as array.
     *
     * @return Array of array
     */
    public function getExportQuery()
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT events_tracking.id, event_type, rep_id, target_id, brands.name as brandname, media.media_code, messages.message_code, territories_id, events_tracking.createdAt FROM events_tracking LEFT JOIN reps ON events_tracking.rep_id = reps.rep_id LEFT JOIN brands ON events_tracking.brand_id = brands.id LEFT JOIN emails on events_tracking.email_id = emails.id LEFT JOIN emails_media_xref on emails_media_xref.media_id = events_tracking.media_id LEFT JOIN emails_messages_xref on emails_messages_xref.emails_id = events_tracking.email_id LEFT JOIN media on media.id = emails_media_xref.media_id LEFT JOIN messages on messages.id = emails_messages_xref.messages_id');
        $stmt->execute();

        return $stmt;
    }
}

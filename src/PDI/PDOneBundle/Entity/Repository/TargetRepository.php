<?php

/**
 * Repository:    Target.
 *
 * @author        Reynier Perez <reynier@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query as DoctrineQuery;

class TargetRepository extends EntityRepository
{
    /**
     * Get targets as array.
     *
     * @return Array of array
     */
    public function getAllTargets()
    {
        return $this->getDoctrine()
            ->getRepository('PDOneBundle:Target')
            ->createQueryBuilder('q')
            ->select('q')
            ->getQuery()
            ->getResult(DoctrineQuery::HYDRATE_ARRAY);
    }

    /**
     * Find targets info in panel_list table and syncs with targets table.
     *
     * @param string $firstname
     * @param string $lastname
     * @param string $phone
     *
     * @return Array of array
     */
    public function findTargetInfoAndSync($firstname, $lastname, $phone)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT * FROM panel_list WHERE LOWER(FIRST_NAME) =  LOWER(:firstname) OR LOWER(LAST_NAME) = LOWER(:lastname) OR TELEPHONE_NUMBER = :phone LIMIT 1');

        $stmt->bindValue('firstname', $firstname);
        $stmt->bindValue('lastname', $lastname);
        $stmt->bindValue('phone', $phone);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets the total shares of the media by reps.
     *
     * @param int $rid
     * @param int $tid
     * @param int $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getTargetActivityAction()
     */
    public function getTotalShares($rid, $tid, $mid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT COUNT(*) from emails_media_xref LEFT JOIN emails on emails.id = emails_media_xref.emails_id WHERE emails.reps_id = :rid AND emails_media_xref.media_id = :mid and emails.targets_id = :tid');

        $stmt->bindValue('rid', $rid);
        $stmt->bindValue('tid', $tid);
        $stmt->bindValue('mid', $mid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the total views of the media by reps.
     *
     * @param int $rid
     * @param int $tid
     * @param int $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getTargetActivityAction()
     */
    public function getTotalViews($rid, $tid, $mid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT COUNT(*) FROM events_tracking WHERE events_tracking.rep_id = :rid AND events_tracking.event_type = "media_view" AND events_tracking.media_id = :mid AND events_tracking.target_id = :tid');

        $stmt->bindValue('rid', $rid);
        $stmt->bindValue('tid', $tid);
        $stmt->bindValue('mid', $mid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the last views of the media by targets and reps.
     *
     * @param int $rid
     * @param int $tid
     * @param int $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getTargetActivityAction()
     */
    public function getLastViewed($rid, $tid, $mid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT events_tracking.createdAt FROM events_tracking WHERE events_tracking.rep_id = :rid AND events_tracking.event_type = "media_view" AND events_tracking.media_id = :mid AND events_tracking.target_id = :tid ORDER BY events_tracking.createdAt DESC LIMIT 1');

        $stmt->bindValue('rid', $rid);
        $stmt->bindValue('tid', $tid);
        $stmt->bindValue('mid', $mid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets the last shared of the media by targets and reps.
     *
     * @param int $rid
     * @param int $tid
     * @param int $mid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getTargetActivityAction()
     */
    public function getLastShared($rid, $tid, $mid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT emails.deliveredAt from emails_media_xref LEFT JOIN emails on emails.id = emails_media_xref.emails_id WHERE emails.reps_id = :rid AND emails_media_xref.media_id = :mid AND emails.targets_id = :tid ORDER BY emails.deliveredAt DESC LIMIT 1');

        $stmt->bindValue('rid', $rid);
        $stmt->bindValue('tid', $tid);
        $stmt->bindValue('mid', $mid);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    /**
     * Gets targets activity.
     *
     * @param int $rid
     * @param int $tid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses getTargetActivityAction()
     */
    public function getTargetsActivity($rid, $tid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT
                            events_tracking.id,
                            events_tracking.event_type,
                            events_tracking.createdAt,
                            targets.display_name,
                            media.title,
                            media.description,
                            emails_messages_xref.emails_id as mid,
                            messages.message_subject,
                            messages.message_text
                        FROM
                            events_tracking
                        LEFT JOIN targets ON (events_tracking.target_id = targets.target_id)
                        LEFT JOIN media ON (events_tracking.media_id = media.id)
                        LEFT JOIN emails ON (events_tracking.email_id = emails.id)
                        LEFT JOIN emails_messages_xref ON (emails.id = emails_messages_xref.emails_id)
                        LEFT JOIN messages ON (messages.id = emails_messages_xref.messages_id)
                        WHERE
                            events_tracking.rep_id = :rid
                        AND events_tracking.target_id = :tid
                        AND (
                            events_tracking.event_type = "email_sent"
                            OR events_tracking.event_type = "email_view"
                            OR events_tracking.event_type = "page_view"
                            OR events_tracking.event_type = "media_view"
                            OR events_tracking.event_type = "rep_contact"
                        )
                        ORDER BY
                            events_tracking.createdAt DESC
                        LIMIT 10');

        $stmt->bindValue('rid', $rid);
        $stmt->bindValue('tid', $tid);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Gets targets lastSyncAt.
     *
     * @param int $tid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses postSessionAction()
     */
    public function getTargetsLastSyncAt($tid)
    {
        $qb = $this->createQueryBuilder('tq');
        $qb->select('t.lastSyncAt')
            ->from('PDOneBundle:Target', 't')
            ->where('t.territory = :tid')
            ->setMaxResults(1)
            ->setParameter('tid', $tid);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Gets targets shared medias.
     *
     * @param int $tid
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @uses BrandPageController:indexAction()
     */
    public function getTargetMedias($tid)
    {
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare('SELECT media.* FROM media LEFT JOIN events_tracking ON media.id = events_tracking.media_id WHERE target_id = :tid GROUP BY events_tracking.media_id');

        $stmt->bindValue('tid', $tid);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}

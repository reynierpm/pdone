<?php

/**
 * Entity:        Target.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Entity;

use Aws\Common\Credentials\Credentials;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use PDI\PDOneBundle\Model\UploadTrait;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="targets", options={"collate"="utf8_general_ci"})
 * @ORM\Entity(repositoryClass="PDI\PDOneBundle\Entity\Repository\TargetRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ExclusionPolicy("all")
 */
class Target
{
    use TimestampableEntity;
    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="string", length=45, nullable=false, unique=true)
     * @Expose()
     */
    protected $target_id;

    use UploadTrait;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Expose()
     */
    protected $veeva_timestamp;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     * @Assert\NotBlank()
     */
    protected $display_name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Expose()
     */
    protected $avatar_url = 'https://pdone.s3.amazonaws.com/avatar/no_avatar.png';

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     * @Expose()
     * @Assert\NotBlank()
     */
    protected $first;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     * @Expose()
     * @Assert\NotBlank()
     */
    protected $last;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $suffix;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Expose()
     */
    protected $address1;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Expose()
     */
    protected $address2;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $city;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $state;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $zip;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $fax;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $state_licensed;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $state_licensed_id;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     * @Expose()
     */
    protected $target_type = 'VEEVA';

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $npi;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $practioner_id;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @Expose()
     */
    protected $inactive = false;

    /**
     * @var Territory
     *
     * @ORM\ManyToOne(targetEntity="Territory")
     * @ORM\JoinColumn(name="territories_id", referencedColumnName="territory_id")
     */
    protected $territory;

    /**
     * @ORM\OneToMany(targetEntity="TargetBrand" , mappedBy="target")
     * */
    protected $targetBrand;

    /**
     * @ORM\OneToMany(targetEntity="TerritoryBrand" , mappedBy="territory")
     * */
    protected $territoryTarget;

    /**
     * Get veevaAccountId.
     *
     * @return string
     */
    public function getVeevaAccountId()
    {
        return $this->target_id;
    }

    /**
     * Set veevaAccountId.
     *
     * @param string $veevaAccountId
     *
     * @return Target
     */
    public function setVeevaAccountId($veevaAccountId)
    {
        $this->target_id = $veevaAccountId;

        return $this;
    }

    /**
     * Get veevaTimestamp.
     *
     * @return \DateTime
     */
    public function getVeevaTimestamp()
    {
        return $this->veeva_timestamp;
    }

    /**
     * Set veevaTimestamp.
     *
     * @param \DateTime $veevaTimestamp
     *
     * @return Target
     */
    public function setVeevaTimestamp($veevaTimestamp)
    {
        $this->veeva_timestamp = $veevaTimestamp;

        return $this;
    }

    /**
     * Get displayName.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * Set displayName.
     *
     * @param string $displayName
     *
     * @return Target
     */
    public function setDisplayName($displayName)
    {
        $this->display_name = $displayName;

        return $this;
    }

    /**
     * Get avatarUrl.
     *
     * @return string
     */
    public function getAvatarUrl()
    {
        return $this->avatar_url;
    }

    /**
     * Set avatarUrl.
     *
     * @param string $avatarUrl
     *
     * @return Target
     */
    public function setAvatarUrl($avatarUrl)
    {
        $this->avatar_url = $avatarUrl;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Target
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get first.
     *
     * @return string
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * Set first.
     *
     * @param string $first
     *
     * @return Target
     */
    public function setFirst($first)
    {
        $this->first = $first;

        return $this;
    }

    /**
     * Get last.
     *
     * @return string
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * Set last.
     *
     * @param string $last
     *
     * @return Target
     */
    public function setLast($last)
    {
        $this->last = $last;

        return $this;
    }

    /**
     * Get suffix.
     *
     * @return string
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * Set suffix.
     *
     * @param string $suffix
     *
     * @return Target
     */
    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;

        return $this;
    }

    /**
     * Get address1.
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address1.
     *
     * @param string $address1
     *
     * @return Target
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address2.
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set address2.
     *
     * @param string $address2
     *
     * @return Target
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Target
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get state.
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state.
     *
     * @param string $state
     *
     * @return Target
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get zip.
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set zip.
     *
     * @param string $zip
     *
     * @return Target
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return Target
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get fax.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set fax.
     *
     * @param string $fax
     *
     * @return Target
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Target
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get stateLicensed.
     *
     * @return string
     */
    public function getStateLicensed()
    {
        return $this->state_licensed;
    }

    /**
     * Set stateLicensed.
     *
     * @param string $stateLicensed
     *
     * @return Target
     */
    public function setStateLicensed($stateLicensed)
    {
        $this->state_licensed = $stateLicensed;

        return $this;
    }

    /**
     * Get stateLicensedId.
     *
     * @return string
     */
    public function getStateLicensedId()
    {
        return $this->state_licensed_id;
    }

    /**
     * Set stateLicensedId.
     *
     * @param string $stateLicensedId
     *
     * @return Target
     */
    public function setStateLicensedId($stateLicensedId)
    {
        $this->state_licensed_id = $stateLicensedId;

        return $this;
    }

    /**
     * Get targetType.
     *
     * @return string
     */
    public function getTargetType()
    {
        return $this->target_type;
    }

    /**
     * Set targetType.
     *
     * @param string $targetType
     *
     * @return Target
     */
    public function setTargetType($targetType)
    {
        $this->target_type = $targetType;

        return $this;
    }

    /**
     * Get npi.
     *
     * @return string
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set npi.
     *
     * @param string $npi
     *
     * @return Target
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get practionerId.
     *
     * @return string
     */
    public function getPractionerId()
    {
        return $this->practioner_id;
    }

    /**
     * Set practionerId.
     *
     * @param string $practionerId
     *
     * @return Target
     */
    public function setPractionerId($practionerId)
    {
        $this->practioner_id = $practionerId;

        return $this;
    }

    /**
     * Get inactive.
     *
     * @return bool
     */
    public function getInactive()
    {
        return $this->inactive;
    }

    /**
     * Set inactive.
     *
     * @param bool $inactive
     *
     * @return Target
     */
    public function setInactive($inactive)
    {
        $this->inactive = $inactive;

        return $this;
    }

    /**
     * Get territory.
     *
     * @return \PDI\PDOneBundle\Entity\Territory
     */
    public function getTerritory()
    {
        return $this->territory;
    }

    /**
     * Set territory.
     *
     * @param \PDI\PDOneBundle\Entity\Territory $territory
     *
     * @return Rep
     */
    public function setTerritory(Territory $territory = null)
    {
        $this->territory = $territory;

        return $this;
    }

    public function __toString()
    {
        return $this->first;
    }

    /**
     * Manages the copying of the file to the relevant place on the server.
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }
        $this->getFile()->move(
            self::$upload_dir,
            $this->getFile()->getClientOriginalName()
        );
        $dir = self::$upload_dir.$this->getFile()->getClientOriginalName();
        $credentials = new Credentials(self::$key, self::$secret_key);
        $s3 = S3Client::factory(array('credentials' => $credentials));
        $uploadFile = sprintf('%s/%s.%s', 'taget', uniqid(), $this->getFile()->getClientOriginalExtension());
        try {
            $resource = fopen($dir, 'r');
            $result = $s3->upload('pdone', $uploadFile, $resource, 'public-read');
            $this->avatar_url = $result['ObjectURL'];
        } catch (S3Exception $e) {
            echo "There was an error uploading the file.\n";
        }
        unlink($dir);
        $this->setFile(null);
    }
}

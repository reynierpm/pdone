<?php

/**
 * Entity:        Territory.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="territories", options={"collate"="utf8_general_ci"})
 * @ORM\Entity(repositoryClass="PDI\PDOneBundle\Entity\Repository\TerritoryRepository")
 * @ExclusionPolicy("all")
 */
class Territory
{
    use TimestampableEntity;

    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\Column(type="string", length=45, nullable=false, unique=true)
     * @Expose()
     */
    protected $territory_id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=45)
     * @Expose()
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    protected $description;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Expose()
     */
    protected $district;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $inactive = false;

    /**
     * @ORM\OneToMany(targetEntity="TerritoryBrand" , mappedBy="territory", cascade={"persist"}, orphanRemoval=true)
     * */
    protected $territoryTarget;

    public function __construct()
    {
        $this->territoryTarget = new ArrayCollection();
    }

    /**
     * Set Veeva Territory.
     *
     * @param string $veeva_territory_id
     *
     * @return Territory
     */
    public function setTerritoryId($veeva_territory_id)
    {
        $this->territory_id = $veeva_territory_id;

        return $this;
    }

    /**
     * Get Veeva Territory.
     *
     * @return string
     */
    public function getTerritoryId()
    {
        return $this->territory_id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Territory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Territory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set district.
     *
     * @param int $district
     *
     * @return Territory
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district.
     *
     * @return int
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set inactive.
     *
     * @param bool $inactive
     *
     * @return Territory
     */
    public function setInactive($inactive)
    {
        $this->inactive = $inactive;

        return $this;
    }

    /**
     * Get inactive.
     *
     * @return bool
     */
    public function getInactive()
    {
        return $this->inactive;
    }

    /**
     * Add territoryTarget.
     *
     * @param TerritoryBrand $translation
     *
     * @return Brands
     */
    public function addTerritoryTarget(TerritoryBrand $territoryTarget)
    {
        $territoryTarget->setTerritory($this); // !important
        $this->territoryTarget[] = $territoryTarget;

        return $this;
    }

    /**
     * Remove territoryTarget.
     *
     * @param TerritoryBrand $territoryTarget
     */
    public function removeTerritoryTarget(TerritoryBrand $territoryTarget)
    {
        $this->territoryTarget->removeElement($territoryTarget);
    }

    /**
     * Get territoryTarget.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTerritoryTarget()
    {
        return $this->territoryTarget;
    }

    public function __toString()
    {
        return $this->name;
    }
}

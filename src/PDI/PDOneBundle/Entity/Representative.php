<?php

/**
 * Entity:        Representative.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Entity;

use Aws\Common\Credentials\Credentials;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use PDI\PDOneBundle\Model\UploadTrait;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="reps", options={"collate"="utf8_general_ci"})
 * @ORM\Entity(repositoryClass="PDI\PDOneBundle\Entity\Repository\RepresentativeRepository")
 * @ExclusionPolicy("all")
 */
class Representative implements UserInterface
{
    use TimestampableEntity;
    use UploadTrait;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=45, nullable=false, unique=true)
     * @Expose()
     */
    protected $rep_id;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     * @Expose()
     * @Assert\NotBlank()
     */
    protected $display_name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, options={"default": "https://pdone.s3.amazonaws.com/avatar/default_avatar.png"})
     * @Expose()
     */
    protected $avatar_url = 'https://pdone.s3.amazonaws.com/avatar/default_avatar.png';

    /**
     * @var string
     * @ORM\Column(type="string", length=45, options={"default": "VEEVA"})
     * @Expose()
     */
    protected $rep_type = 'VEEVA';

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     * @Expose()
     * @Assert\NotBlank()
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     * @Expose()
     * @Assert\NotBlank()
     */
    protected $first;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     * @Expose()
     * @Assert\NotBlank()
     */
    protected $last;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $bio;

    /**
     * @var string
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Expose()
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(type="string", length=45)
     * @Expose()
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": false})
     * @Expose()
     */
    protected $inactive = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Expose()
     */
    protected $lastLoginAt;

    /**
     * @var Territory
     *                ORM\Column(type="varchar", length=45, nullable=true)
     * @ORM\ManyToOne(targetEntity="Territory")
     * @ORM\JoinColumn(name="territories_id", referencedColumnName="territory_id")
     * @Expose()
     */
    protected $territory;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, length=150, unique=true)
     */
    protected $repTokenId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, length=150)
     */
    protected $deviceToken;

    /**
     * Get FullName.
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->first.' '.$this->last;
    }

    /**
     * Get veevaRepId.
     *
     * @return string
     */
    public function getVeevaRepId()
    {
        return $this->rep_id;
    }

    /**
     * Set veevaRepId.
     *
     * @param string $veevaRepId
     *
     * @return Representative
     */
    public function setVeevaRepId($veevaRepId)
    {
        $this->rep_id = $veevaRepId;

        return $this;
    }

    /**
     * Get displayName.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * Set displayName.
     *
     * @param string $displayName
     *
     * @return Representative
     */
    public function setDisplayName($displayName)
    {
        $this->display_name = $displayName;

        return $this;
    }

    /**
     * Get avatarUrl.
     *
     * @return string
     */
    public function getAvatarUrl()
    {
        return $this->avatar_url;
    }

    /**
     * Set avatarUrl.
     *
     * @param string $avatarUrl
     *
     * @return Representative
     */
    public function setAvatarUrl($avatarUrl)
    {
        $this->avatar_url = $avatarUrl;

        return $this;
    }

    /**
     * Get repType.
     *
     * @return string
     */
    public function getRepType()
    {
        return $this->rep_type;
    }

    /**
     * Set repType.
     *
     * @param string $repType
     *
     * @return Rep
     */
    public function setRepType($repType)
    {
        $this->rep_type = $repType;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return Representative
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get first.
     *
     * @return string
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * Set first.
     *
     * @param string $first
     *
     * @return Representative
     */
    public function setFirst($first)
    {
        $this->first = $first;

        return $this;
    }

    /**
     * Get last.
     *
     * @return string
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * Set last.
     *
     * @param string $last
     *
     * @return Representative
     */
    public function setLast($last)
    {
        $this->last = $last;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Representative
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get bio.
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set bio.
     *
     * @param string $bio
     *
     * @return Representative
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return Representative
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Representative
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get inactive.
     *
     * @return bool
     */
    public function getInactive()
    {
        return $this->inactive;
    }

    /**
     * Set inactive.
     *
     * @param bool $inactive
     *
     * @return Representative
     */
    public function setInactive($inactive)
    {
        $this->inactive = $inactive;

        return $this;
    }

    /**
     * Get lastloginat.
     *
     * @return \DateTime
     */
    public function getLastLoginAt()
    {
        return $this->lastLoginAt;
    }

    /**
     * Set lastloginat.
     *
     * @param \DateTime $lastloginat
     *
     * @return Representative
     */
    public function setLastLoginAt(\DateTime $lastloginat)
    {
        $this->lastLoginAt = $lastloginat;

        return $this;
    }

    /**
     * Get territory.
     *
     * @return \PDI\PDOneBundle\Entity\Territory
     */
    public function getTerritory()
    {
        return $this->territory;
    }

    /**
     * Set territory.
     *
     * @param \PDI\PDOneBundle\Entity\Territory $territory
     *
     * @return Representative
     */
    public function setTerritory(Territory $territory = null)
    {
        $this->territory = $territory;

        return $this;
    }

    /**
     * Set reps token id.
     *
     * @param string $repTokenId
     *
     * @return string
     */
    public function setRepTokenId($repTokenId)
    {
        $this->repTokenId = $repTokenId;

        return $this;
    }

    /**
     * Get reps token id.
     *
     * @return string
     */
    public function getRepTokenId()
    {
        return $this->repTokenId;
    }

    /**
     * Set device token.
     *
     * @param string $deviceToken
     *
     * @return string
     */
    public function setDeviceToken($deviceToken)
    {
        $this->deviceToken = $deviceToken;

        return $this;
    }

    /**
     * Get device token.
     *
     * @return string
     */
    public function getDeviceToken()
    {
        return $this->deviceToken;
    }

    public function __toString()
    {
        return $this->display_name;
    }

    /**
     * Manages the copying of the file to the relevant place on the server.
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }
        $this->getFile()->move(
            self::$upload_dir,
            $this->getFile()->getClientOriginalName()
        );
        $dir = self::$upload_dir.$this->getFile()->getClientOriginalName();
        $credentials = new Credentials(self::$key, self::$secret_key);
        $s3 = S3Client::factory(array('credentials' => $credentials));
        $uploadFile = sprintf('%s/%s.%s', 'representative', uniqid(), $this->getFile()->getClientOriginalExtension());
        try {
            $resource = fopen($dir, 'r');
            $result = $s3->upload('pdone', $uploadFile, $resource, 'public-read');
            $this->avatar_url = $result['ObjectURL'];
        } catch (S3Exception $e) {
            echo "There was an error uploading the file.\n";
        }
        unlink($dir);
        $this->setFile(null);
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return array('ROLE_REPRESENTATIVE');
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        return;
    }
}

<?php

namespace PDI\PDOneBundle\Security;

use KnpU\Guard\AbstractGuardAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ApiTokenAuthenticator.
 *
 * {@inheritdoc}
 */
class ApiTokenAuthenticator extends AbstractGuardAuthenticator
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getCredentials(Request $request)
    {
        if ($request->attributes->get('_route') == 'api_1_post_session' || $request->attributes->get('_route') == 'api_1_post_syncs' || $request->attributes->get('_route') == 'nelmio_api_doc_index' || $request->attributes->get('_route') == 'api_1_get_tracking_email') {
            return;
        } elseif ($request->headers->get('X-PDONE-SESSION-ID') === null || $request->headers->get('X-PDONE-SESSION-ID') === '') {
            return new JsonResponse(
                array('message' => 'Invalid X-PDONE-SESSION-ID'),
                403
            );
        }

        return $request->headers->get('X-PDONE-SESSION-ID');
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = $this->em->getRepository('PDOneBundle:Representative')
            ->findOneBy(array('repTokenId' => $credentials));

        // we could just return null, but this allows us to control the message a bit more
        if (!$user) {
            throw new AuthenticationCredentialsNotFoundException();
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // the fact that they had a valid token that *was* attached to a user
        // means that their credentials are correct. So, there's nothing
        // additional (like a password) to check here.
        return;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->headers->get('X-PDONE-SESSION-ID') === null) {
            return new JsonResponse(
                array('message' => 'Invalid X-PDONE-SESSION-ID'),
                403
            );
        }

        return new JsonResponse(
        // you could translate the message
            array('message' => $exception->getMessageKey()),
            403
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // do nothing - let the request just continue!
        return;
    }

    public function supportsRememberMe()
    {
        return false;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse(
        // you could translate the message
            array('message' => 'Authentication required'),
            401
        );
    }
}

<?php

/**
 * Trait:         UploadTrait.
 *
 * @author        Pedro Hernandez <pedro@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;

trait UploadTrait
{
    private static $key = 'AKIAJRNDR5O3FC66AIEA';
    private static $secret_key = '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';
    private static $upload_dir = '/var/www/html/mt020415-00-pdonereptool/web/uploads/';

    private $file;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
}

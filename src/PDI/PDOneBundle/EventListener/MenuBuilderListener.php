<?php

/**
 * Menu:          MenuBuilder.
 *
 * @author        Reynier Perez <reynier@magnifictech.com>
 * @copyright (c) Magnific Technology LLC
 */

namespace PDI\PDOneBundle\EventListener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;

class MenuBuilderListener
{
    public function createMainMenu(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $menu
            ->addChild('Dashboard', array('uri' => '/'))
            ->setAttribute('icon', 'fa fa-home');

        $menu
            ->addChild('Company', array())
            ->setAttribute('icon', 'fa fa-inbox')
            ->addChild(
                'Company',
                array(
                    'route' => 'admin_pdi_pdone_company_list',
                )
            )
            ->setAttribute('icon', 'fa fa-inbox')
            ->getParent()
            ->addChild(
                'Brand',
                array(
                    'route' => 'admin_pdi_pdone_brand_list',
                )
            )
            ->setAttribute('icon', 'fa fa-inbox')
            ->addChild(
                'Media',
                array(
                    'route' => 'admin_pdi_pdone_media_list',
                )
            )
            ->setAttribute('icon', 'fa fa-inbox')
            ->getParent();
    }
}
